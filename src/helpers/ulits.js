const _ = require('lodash');
export function isMyAccount(id){
    let user = JSON.parse(localStorage.getItem('user'));
    if(_.isUndefined(user.member.id)){
        return false;
    }
    if(user.member.id == id){
        return true;
    }
    return false;
}
export function isAdmin(){
    let user = JSON.parse(localStorage.getItem('user'));
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"admin") > -1){
        return true;
    }
    return false;
}
export function isManager(){
    let user = JSON.parse(localStorage.getItem('user'));
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"manager") > -1){
        return true;
    }
    return false;
}
export function isReport(){
    let user = JSON.parse(localStorage.getItem('user'));
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"report") > -1){
        return true;
    }
    return false;
}

export function defaultUserRole(){
    let user = JSON.parse(localStorage.getItem('user'));
    //console.log(user);
    let role = false;
    if(_.isUndefined(user.roles)){
        role = false;
    }
    role = user.roles[0];

    if(role && role == "admin" && isAdmin()){
        return "admin";
    }
    if(role && role == "manager" && isManager()){
        return "manager";
    }
    if(role && role == "report" && isReport()){
        return "report";
    }
}

export function canUseCms(){
    return (isAdmin() || isManager() || isReport())
}