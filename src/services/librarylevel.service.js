import { authHeader } from '../helpers';
import config from '../config';
import { userService } from './user.service';

export const librarylevelService = {
    save,
    getById,
    update,
    all,
    update 
};
function all(filter){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    if(!filter){
        filter = {
            "include": []
        };
    }
    
    return fetch(config.apiUrl+"/library-levels?filter="+JSON.stringify(filter), requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function getById(id){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    let filter = {
        "include": []
    };
    return fetch(config.apiUrl+`/library-levels/`+id+"?filter="+JSON.stringify(filter), requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function update(data){
    const requestOptions = {
        method: 'PATCH',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/library-levels/`+data.id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function save(data){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/library-levels`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data.error && data.error.message) || response.statusText;
            return Promise.reject(error);
        }
        if(data.status === false){
            const error = data.message;
            return Promise.reject(error);
        }
        return data;
    });
}