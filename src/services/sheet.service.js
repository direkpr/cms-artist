import { authHeader } from '../helpers';
import config from '../config';
import { userService,defaultService } from './';
const _ = require('lodash');

const modelName = "library-sheets";
export const sheetService = {
    save,
    getById : function(id){
        const requestOptions = {
            method: 'GET',
            headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        };
        let filter = {
            "include": [{"relation":"sheetFile"},{"relation":"sheetVideos","scope":{"order":["order_item asc"]}}]
        };
        return fetch(config.apiUrl+`/${modelName}/`+id+"?filter="+JSON.stringify(filter), requestOptions)
            .then(handleResponse)
            .then(data => {
                return data;
            });
    },
    all : function(filter){
        return defaultService.all(filter,modelName);
    },
    update,
    getSheetExercise,
    saveExercises
};
function saveExercises(id,exercises){
    exercises = _.sortBy(exercises,["ex_no"]);
    return new Promise((resolve,reject)=>{
        fetch(config.apiUrl+`/library-sheets/${id}/sheet-exercises`, {
            method: 'DELETE',
            headers: {...{ 'Content-Type': 'application/json' },...authHeader()}
        })
        .then(response => {
            for(let i =0; i < exercises.length; i++){
                delete exercises[i].id;
                exercises[i].ex_no =i;
                exercises[i].librarySheetId = parseInt(exercises[i].librarySheetId);
                fetch(config.apiUrl+`/library-sheets/${id}/sheet-exercises`, {
                    method: 'POST',
                    headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
                    body: JSON.stringify(exercises[i])
                })
            }
            resolve(response);
        });
    });
}
function getSheetExercise(id){
    return fetch(config.apiUrl+`/library-sheets/${id}/sheet-exercises`, {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()}
    })
    .then(handleResponse)
    .then(data => {
        return data;
    });
}
function save(data,sheetfile,videos){
    return new Promise((resolve,reject)=>{
        defaultService.save(data,modelName).then((response)=>{
            //console.log(response);
            if(response && response.id){
                if(!(_.isNull(sheetfile)|| _.isUndefined(sheetfile))){
                    let file = {
                        name : sheetfile.originalname,
                        path : sheetfile.link,
                        file_type : sheetfile.mimetype,
                        librarySheetId : response.id,
                        remark: "",
                        createdtime : data.createdtime,
                        updatedtime : data.updatedtime
                    };
                    fetch(config.apiUrl+`/library-sheets/${response.id}/sheet-file`, {
                        method: 'POST',
                        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
                        body: JSON.stringify(file)
                    });
                }
                _.each(videos,(video,vi)=>{
                    let datasearch = "";
                    datasearch += video.title;
                    datasearch += video.link;
                    datasearch += video.tags.toString().replace(",",'');
                    datasearch = datasearch.replace(/\s/g, '');
                    datasearch = datasearch.toLocaleLowerCase();

                    let obj = {
                        title : video.title,
                        link : video.link,
                        imgUrl : video.imgUrl,
                        librarySheetId : response.id,
                        tag : video.tags.toString(),
                        datasearch : datasearch,
                        createdtime : data.createdtime,
                        updatedtime : data.updatedtime,
                        order_item : vi
                    }
                    fetch(config.apiUrl+`/library-sheets/${response.id}/sheet-videos`, {
                        method: 'POST',
                        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
                        body: JSON.stringify(obj)
                    });
                });
                resolve(response);
            }
        },(error)=>{
            reject(error);
        });
    })
}
function update(data,sheetfile,videos,sheetVideos){
    //console.log(sheetfile);
    return new Promise((resolve,reject)=>{
        defaultService.update(data,modelName).then((response)=>{

            //add new sheetfile
            if(!(_.isNull(sheetfile)|| _.isUndefined(sheetfile))){
                if(_.isUndefined(sheetfile.id)){
                    let file = {
                        name : sheetfile.name,
                        path : sheetfile.path,
                        file_type : sheetfile.mimetype,
                        librarySheetId : data.id,
                        remark: "",
                        createdtime : data.createdtime,
                        updatedtime : data.updatedtime
                    };
                    fetch(config.apiUrl+`/library-sheets/${data.id}/sheet-file`, {
                        method: 'POST',
                        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
                        body: JSON.stringify(file)
                    });
                }
            }
            

            //check delet video
            _.each(videos,(video,k)=>{
                video.order_item = k;
            })
            //console.log(videos);
            let diff_videos = _.xor(videos,sheetVideos);
            //console.log([videos,sheetVideos]);
            //console.log(diff_videos);
            let del_videos = _.without(diff_videos,videos);
            //console.log(del_videos);
            _.each(videos,(video,k)=>{
                if(!_.isUndefined(video)){
                    if(_.isUndefined(video.id)){
                        //add new video
                        let datasearch = "";
                        datasearch += video.title;
                        datasearch += video.link;
                        datasearch += video.tags.toString().replace(",",'');
                        datasearch = datasearch.replace(/\s/g, '');
                        datasearch = datasearch.toLocaleLowerCase();
    
                        let obj = {
                            title : video.title,
                            link : video.link,
                            imgUrl : video.imgUrl,
                            librarySheetId : data.id,
                            tag : video.tags.toString(),
                            datasearch : datasearch,
                            createdtime : data.createdtime,
                            updatedtime : data.updatedtime,
                            order_item : video.order_item
                        }
                        fetch(config.apiUrl+`/library-sheets/${data.id}/sheet-videos`, {
                            method: 'POST',
                            headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
                            body: JSON.stringify(obj)
                        });
                        
                    }else{
                        //update video
                        let datasearch = "";
                        datasearch += video.title;
                        datasearch += video.link;
                        datasearch += video.tags.toString().replace(",",'');
                        datasearch = datasearch.replace(/\s/g, '');
                        datasearch = datasearch.toLocaleLowerCase();
    
                        let obj = {
                            id : video.id,
                            title : video.title,
                            link : video.link,
                            imgUrl : video.imgUrl,
                            librarySheetId : data.id,
                            tag : video.tags.toString(),
                            datasearch : datasearch,
                            updatedtime : data.updatedtime,
                            order_item : k
                        }
                        let where = {
                            id : video.id
                        };
                        fetch(config.apiUrl+`/library-sheets/${data.id}/sheet-videos?where=`+JSON.stringify(where), {
                            method: 'PATCH',
                            headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
                            body: JSON.stringify(obj)
                        });
                    }
                }
            });
            //delete old video
            _.each(del_videos,(video,k)=>{
                if(!_.isUndefined(video) && !_.isUndefined(video.id)){
                    let where = {
                        id : video.id
                    };
                    fetch(config.apiUrl+`/library-sheets/${data.id}/sheet-videos?where=`+JSON.stringify(where), {
                        method: 'DELETE',
                        headers: {...{ 'Content-Type': 'application/json' },...authHeader()}
                    });
                }
            })
            resolve(response);
        });
    });
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data.error && data.error.message) || response.statusText;
            return Promise.reject(error);
        }
        if(data.status === false){
            const error = data.message;
            return Promise.reject(error);
        }
        return data;
    });
}