import { authHeader } from '../helpers';
import config from '../config';
import { userService } from './user.service';

export const reportService = {
    member_list,
    recurring_list,
    renew_list,
    ebirthday_list,
    contact_list
};
function contact_list(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/contact?limit=1000000&offset=0", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function ebirthday_list(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/members/ebirthday-list", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function renew_list(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/members/renew-list", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function recurring_list(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/members/recurring", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function member_list(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/members", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data.error && data.error.message) || response.statusText;
            return Promise.reject(error);
        }
        if(data.status === false){
            const error = data.message;
            return Promise.reject(error);
        }
        return data;
    });
}