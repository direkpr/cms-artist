import { authHeader } from '../helpers';
import config from '../config';
const _ = require('lodash');

export const userService = {
    login,
    logout,
    saveUser,
    updateUser,
    getById
}
function updateUser(data){
    data.updatedtime = new Date().toISOString();
    return fetch(config.apiUrl+`/api/admin/members/${data.id}`, {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    })
    .then(handleResponse)
    .then(data => {
        return data;
    });
}
function saveUser(data){
    data.createdtime = new Date().toISOString();
    data.updatedtime = new Date().toISOString();
    return fetch(config.apiUrl+`/api/admin/members`, {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    })
    .then(handleResponse)
    .then(data => {
        return data;
    });
}
function getById(id){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    return fetch(config.apiUrl+`/api/admin/members/`+id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };
    return fetch(config.apiUrl+`/api/login`, requestOptions)
        .then((response)=>{
            return response.text().then(text => {
                const data = text && JSON.parse(text);
                console.log(data);
                if (!response.ok) {
                    if (response.status === 401) {
                        // auto logout if 401 response returned from api
                        logout();
                        return Promise.reject("Invalid username or password.");
                    }
                    const error = (data && data.message) || response.statusText;
                    return Promise.reject(error);
                }
                if(data.status == false){
                    return Promise.reject(data.message);
                }
                return data;
            });
        })
        .then(user => {
            // login successful if there's a user in the response
            if (user) {
                // store user details and basic auth credentials in local storage 
                // to keep user logged in between page refreshes
                user.authdata = window.btoa(email + ':' + password);
                localStorage.setItem('user', JSON.stringify(user));
            }
            return user;
        });
}
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}