import { authHeader } from '../helpers';
import config from '../config';
import { userService } from './user.service';

export const defaultService = {
    save,
    getById,
    update,
    all,
    update ,
    setActive,
    get_langs,
    updateOrdeItem
};
function updateOrdeItem(id,order_item,model){
    //console.log([id,order_item,model])
    const requestOptions = {
        method: 'PATCH',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({order_item,id})
    };
    return fetch(config.apiUrl+`/api/${model}/`+id+"/change-order", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function get_langs(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/langs?isActive=1", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function setActive(id,isActive,model){
    const requestOptions = {
        method: 'PATCH',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({isActive,id})
    };
    return fetch(config.apiUrl+`/api/${model}/`+id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function all(model,query){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api/"+model+"?"+query, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function getById(id,model){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    return fetch(config.apiUrl+`/api/${model}/`+id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function update(data,model){
    const requestOptions = {
        method: 'PATCH',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/${model}/`+data.id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function save(data,model){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/${model}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data.error && data.error.message) || response.statusText;
            return Promise.reject(error);
        }
        if(data.status === false){
            const error = data.message;
            return Promise.reject(error);
        }
        return data;
    });
}