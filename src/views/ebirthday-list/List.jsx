import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Icon from '@material-ui/core/Icon';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {authHeader,isAdmin} from '../../helpers'
import { FaEyeSlash,FaEye } from "react-icons/fa";
import * as moment from 'moment-timezone';
import {defaultService as service,reportService} from '../../services';
import { blue } from '@material-ui/core/colors';
import SignUpList from './SignUpList';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Paper } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';

const _ = require('lodash');
const modelName ="albums";
class PageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            startDate :null,
            endDate : null,
            members : [],
            data : []
        }
        this.tableRef = React.createRef();
        this.handleChange = this.handleChange.bind(this);
        this.updateData = this.updateData.bind(this);
        moment.tz.setDefault("Asia/Bangkok");
    }
    updateData(){
        let {startDate,endDate,members} = this.state;
        let data = members;
        if(!_.isNull(startDate)){
            let sDate = moment(startDate+" 00:00:00");
            data = _.filter(data,(o)=>{
                let cDate = moment(o.createdtime);
                return (cDate >= sDate);
            })
        }
        if(!_.isNull(endDate)){
            let eDate = moment(endDate+" 23:59:59");
            data = _.filter(data,(o)=>{
                let cDate = moment(o.createdtime);
                return (cDate <= eDate);
            })
        }
        this.setState({data});
    }
    handleChange(e){
        const { name, value,checked } = e.target;
        //console.log([name,value]);
        this.setState({ [name]: value });
        setTimeout(this.updateData,300);
    }
    componentDidMount(){
        reportService.ebirthday_list().then((data)=>{
            //console.log(data);
            this.setState({members:data,data});
        })
    }
    render(){
        let {startDate,endDate,data} = this.state;
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Paper variant="outlined" style={{padding:15,marginBottom:10}} >
                                <Typography component="h2">FILTER BY </Typography>
                                <Row>
                                    <Col md={6}>
                                    <TextField fullWidth
                                        id="startdate"
                                        label="Start Date"
                                        type="date"
                                        name="startDate"
                                        defaultValue={startDate}
                                        onChange={this.handleChange}
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                    </Col>
                                    <Col md={6}>

                                    <TextField fullWidth
                                        id="enddate"
                                        label="End Date"
                                        type="date"
                                        name="endDate"
                                        defaultValue={endDate}
                                        onChange={this.handleChange}
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                    </Col>
                                </Row>
                                
                            </Paper>
                            <SignUpList data={data} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageList;