import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { Editor } from "@tinymce/tinymce-react";

import {defaultService as service} from '../../services';
import config from '../../config';
import { authHeader } from "../../helpers";
import FormHelperText from '@material-ui/core/FormHelperText';

const _ = require('lodash');

const modelName ="news";

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
}
function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box style={{paddingLeft:15,paddingRight:15}} p={3}>
            <Typography component="div">{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            isActive : true,
            category_id : -1,
            release_date : moment().format("YYYY-MM-DD"),
            lg : 0,
            langs : null,
            texts : [],
            shareContent : "",
            imgShare:null
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.tabsHandleChange = this.tabsHandleChange.bind(this);

        this.iconRef1 = React.createRef();
        this.iconRef2 = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "isActive"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,year,submitted,isActive,category_id,release_date,texts,langs,shareContent,imgShare} = this.state;

        if(!release_date){
            confirmAlert({
                title: 'Error Message!',
                message: 'Release date  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(category_id === -1){
            confirmAlert({
                title: 'Error Message!',
                message: 'Category  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let data = {
            id : id,
            release_date : release_date,
            category_id : category_id || -1,
            isActive : isActive,
            texts : texts,
            shareContent: shareContent,
            imgShare : imgShare
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Save data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            //window.location = this.props.path+"/edit/"+response.id;
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => {
                this.setState({ error, submitted: false });
                confirmAlert({
                    title: 'Error Message!',
                    message: 'Save data fail!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                        }
                    }
                    ]
                });
            });
        }
        
    }

    handleReset(e){
        let {langs} = this.state;
        let texts = [];
        _.each(langs,(lang,key)=>{
            texts[key] = {lg:lang.code};
        })
        this.setState({
            isActive : true,
            release_date : moment().format("YYYY-MM-DD"),
            category_id : -1,
            texts:texts
        });
        this.iconRef1.current.onReset();
        this.componentDidMount();
    }
    tabsHandleChange(event,value){
        this.setState({lg:value});
    }
    componentDidMount(){
        service.get_langs().then(langs=>{
            //console.log(langs);
            let texts = [];
            _.each(langs,(lang,key)=>{
                texts[key] = {lg:lang.code};
            })
            this.setState({langs,texts});
            service.getById(this.props.match.params.id,modelName).then((data)=>{
                _.each(langs,(lang,key)=>{
                    let idx = _.find(data.texts,["lg",lang.code]);
                    if(_.isUndefined(idx)){
                        data.texts.push({lg:lang.code})
                    }
                })
                data.isActive = (data.isActive == 1) ? true : false;
                this.setState(data);
            })
        })
    }
    render(){
        const {id,submitted,error,isActive,category_id,release_date,lg,langs,texts,imgShare,shareContent} = this.state;
        if(!langs || !id){
            return (<></>);
        }
        //console.log(texts);
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                
                                    <CardHeader title="UPDATE NEWS"></CardHeader>
                                    
                                    <CardContent>
                                        <Paper square>
                                        <Tabs
                                            value={lg}
                                            indicatorColor="primary"
                                            textColor="primary"
                                            variant="fullWidth"
                                            onChange={this.tabsHandleChange}
                                        >
                                            {
                                                (langs) && langs.map((lang,key)=>(
                                                    <Tab key={key} label={lang.title} {...a11yProps(key)} />
                                                ))
                                            }
                                            
                                        </Tabs>
                                        {
                                            (langs) && langs.map((lang,key)=>(
                                                <TabPanel key={key} value={lg} index={key}>
                                                    <Row>
                                                        <Col md={12} lg={12}>
                                                            <TextField required={(lang.isDefault == 1)} style={{width:'100%',marginTop:10}} defaultValue={texts[key].title} name="title" onChange={(e)=>{
                                                                texts[key].title = e.target.value;
                                                                this.setState({texts});
                                                            }} label="TITLE" />
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col md={12} lg={12}>
                                                            <FormControl style={{width:'100%',marginTop:10}}>
                                                                <Editor style={{marginTop:50}}
                                                                    apiKey={config.tinyKey}
                                                                    initialValue={texts[key].content}
                                                                    onEditorChange={(value)=>{
                                                                        texts[key].content = value;
                                                                        this.setState({texts});
                                                                    }}
                                                                    init={{
                                                                        height: 500,
                                                                        menubar: false,
                                                                        relative_urls : false,
                                                                        remove_script_host : false,
                                                                        images_upload_handler : function(blobInfo, success, failure, progress){
                                                                            var formData = new FormData();
                                                                            formData.append('file', blobInfo.blob(), blobInfo.filename());
                                                                            fetch(config.apiUrl+"/api/upload-file", {
                                                                                method: 'POST',
                                                                                body: formData,
                                                                                headers : authHeader()
                                                                            }).then((response) =>{
                                                                                response.text().then(text =>{
                                                                                    let json = JSON.parse(text);
                                                                                    success(config.imgUrl+json.link); 
                                                                                });
                                                                            })
                                                                        },
                                                                        plugins: [
                                                                        'advlist autolink lists link image charmap print preview anchor',
                                                                        'searchreplace visualblocks code fullscreen',
                                                                        'insertdatetime media table paste code help wordcount'
                                                                        ],
                                                                        toolbar:
                                                                        'undo redo | formatselect | bold italic backcolor | \
                                                                        alignleft aligncenter alignright alignjustify | \
                                                                        bullist numlist outdent indent | removeformat | image media link table code'
                                                                    }}
                                                                />
                                                            </FormControl>
                                                            
                                                        </Col>
                                                    </Row>
                                                </TabPanel>
                                            ))
                                        }

                                            <Row style={{padding:15,paddingBottom:50}}>
                                                <Col md={6} lg={6}>
                                                    <FormControl style={{width:'100%',marginTop:10}}>
                                                        <InputLabel>CATEGORY *</InputLabel>
                                                        <Select required={true}
                                                        name="category_id"
                                                        value={category_id}
                                                        onChange={this.handleChange}
                                                        >
                                                            <MenuItem value={-1}><em>None</em></MenuItem>
                                                            <MenuItem  value={1}>News</MenuItem>
                                                            <MenuItem  value={2}>Live/Event</MenuItem>
                                                            <MenuItem  value={3}>Movies</MenuItem>
                                                        </Select>
                                                    </FormControl>
                                                </Col>
                                                <Col md={6} lg={6}>
                                                    <TextField required={true} 
                                                        type="date" 
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        style={{width:'100%',marginTop:10}} defaultValue={release_date} name="release_date" onChange={this.handleChange} label="RELEASE DATE" />
                                                </Col>
                                            </Row>

                                            <Row style={{padding:15}}>
                                                <Col md={12} lg={12}>
                                                    <TextField  multiline={true} rows={5} style={{width:'100%',marginTop:10}} defaultValue={shareContent} name="shareContent" onChange={this.handleChange} label="SHARE CONTENT" />
                                                </Col>
                                            </Row>
                                            <Row style={{padding:15}}>
                                                <Col md={12}>
                                                    <FormGroup controlId="formControlsIcon">
                                                        <label>SHARE IMAGE</label>
                                                        <IconUpload 
                                                            ref={this.iconRef1}
                                                            defaultValue={imgShare}
                                                            onSuccess={(image)=>{
                                                                this.setState({imgShare:image.link});
                                                            }}
                                                        ></IconUpload>
                                                        <FormHelperText>Image type: JPG file / Size: not over 1 mb / Dimension: width 600 px height 315 px</FormHelperText>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                         </Paper>


                                        
                                        
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={isActive}
                                                            onChange={this.handleChange}
                                                            name="isActive"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                </FormGroup>
                                                
                                            </Col>
                                        </Row>
                                    </CardContent>
                                    <CardActions style={{padding:15}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;