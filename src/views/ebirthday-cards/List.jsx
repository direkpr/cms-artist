import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Icon from '@material-ui/core/Icon';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {authHeader,isAdmin} from '../../helpers'
import { FaEyeSlash,FaEye } from "react-icons/fa";
import * as moment from 'moment';
import {defaultService as service} from '../../services';
import { blue } from '@material-ui/core/colors';

import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import BoxBanners from './BoxBanners';

const _ = require('lodash');
const modelName ="ebirthday-cards";
class PageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            banners : []
        }
        this.tableRef = React.createRef();
    }
    componentDidMount(){
        service.all(modelName,"").then((banners)=>{
            this.setState({banners});
        })
    }
    render(){
        let {banners} = this.state;
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Toolbar style={{float:'right'}}>
                                <Button startIcon={<AddCircleIcon />} onClick={()=>{
                                    window.location = this.props.path+"/add";
                                }} color="inherit">ADD CARD</Button>
                            </Toolbar>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <BoxBanners data={banners} onSetVisible={(item)=>{
                                //console.log(item);
                                let isActive = true;
                                if(item.isActive == "1"){
                                    isActive = false;
                                }
                                service.setActive(item.id,isActive,modelName).then(()=>{
                                    this.componentDidMount();
                                })
                            }} onOrderChange={(newOrder)=>{
                                
                            }} onDelete={(item)=>{
                                confirmAlert({
                                    //title: 'Confirm Box!',
                                    message: 'Do you want to delete "'+item.title+'" ?',
                                    buttons: [
                                      {
                                        label: 'Yes',
                                        onClick: () => {
                                            fetch(config.apiUrl+'/api/'+modelName+'/'+item.id, {
                                                method: 'DELETE',
                                                headers : authHeader()
                                            }).then((response) =>{
                                                response.text().then(text =>{
                                                    this.componentDidMount();
                                                });
                                            })
                                        }
                                      },
                                      {
                                        label: 'No',
                                        onClick: () => {}
                                      }
                                    ]
                                });
                            }} onEdit={(item)=>{
                                window.location = this.props.path+"/edit/"+item.id;
                            }} title="All Cards" />
                        </Col>
                    </Row>
                </Grid>
                
            </div>
        )
    }
}
export default PageList;