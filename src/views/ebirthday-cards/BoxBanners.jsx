import React, { useEffect,useState } from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { ReactSortable } from "react-sortablejs";
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
import config from '../../config';
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow:1,
        flexDirection:"row",
        display:"flex"
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: theme.spacing(32),
        height: 'auto',
        margin : theme.spacing(1),
        cursor : "pointer"
    },
    paperSelected: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: theme.spacing(32),
        height: 'auto',
        margin : theme.spacing(1),
        border: "2px solid #c8c8c8",
        cursor : "pointer"
    },
}));

const BoxBanners = (props) => {
    let {title,data,selected} = props;
    const classes = useStyles();
    if(!data){
        return (<></>);
    }
    return (
        <Card style={{marginTop:20}}>
            <CardHeader
                subheader={title}
            />
            <CardContent>
                <ReactSortable disabled={true} className={classes.root} list={data} setList={(v)=>{
                    if(props.onOrderChange){
                        props.onOrderChange(v);
                    }
                }}>
                {
                    (data && data.map((lib,key)=>{
                        //console.log(lib);
                        return (
                            <Paper onClick={(e)=>{
                                if(props.onSelected){
                                    props.onSelected(lib);
                                }
                            }} key={key} className={classes.paper}>
                            
                            <Grid container style={{height:'100%',minHeight:'100%'}}>
                                    <Grid item xs  style={{display:'flex',flex:1,flexDirection:'column',height:'100%',minHeight:'100%'}}>
                                    <Grid item xs style={{flex:1}}>
                                        <img src={config.imgUrl+lib.imgUrl} style={{width:'100%',height:'auto'}} />
                                        </Grid>
                                        <Grid item xs style={{flex:1}}>
                                            {lib.title}
                                        </Grid>
                                        <Grid item  style={{flexGrow:'none',width:'100%',textAlign:'right'}}>
                                            <IconButton onClick={(e)=>{
                                                if(props.onSetVisible){
                                                    props.onSetVisible(lib);
                                                }
                                            }} aria-label={(lib.isActive == "1") ? "Active" : "Not Active"} className={classes.margin} size="small">
                                                {
                                                    (lib.isActive == "1") && (<VisibilityIcon fontSize="inherit" />)
                                                }
                                                {
                                                    (lib.isActive != "1") && (<VisibilityOffIcon fontSize="inherit" />)
                                                }
                                            </IconButton>
                                            <IconButton onClick={(e)=>{
                                                if(props.onEdit){
                                                    props.onEdit(lib);
                                                }
                                            }} aria-label="Edit" className={classes.margin} size="small">
                                                <EditIcon fontSize="inherit" />
                                            </IconButton>
                                            <IconButton onClick={(e)=>{
                                                if(props.onDelete){
                                                    props.onDelete(lib);
                                                }
                                            }} aria-label="delete" className={classes.margin} size="small">
                                                <DeleteIcon fontSize="inherit" />
                                            </IconButton>
                                        </Grid>
                                        
                                    </Grid>
                                </Grid>
                            </Paper>
                        )
                    }))
                }
                </ReactSortable>
            </CardContent>
        </Card>
    )
}
export default BoxBanners;