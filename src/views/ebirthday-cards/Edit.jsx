import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import {defaultService as service} from '../../services';
import {lightGreen} from '@material-ui/core/colors';
const _ = require('lodash');

const modelName ="ebirthday-cards";
class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            isActive : true,
            imgUrl : undefined,
            title : undefined,
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);

        this.iconRef1 = React.createRef();
        this.iconRef2 = React.createRef();
    }
    componentDidMount(){
        service.getById(this.props.match.params.id,modelName).then(data=>{
            //console.log(data);
            data.isActive = (data.isActive == 1) ? true : false;
            this.setState(data)
        })
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "isActive"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,title,submitted,isActive,imgUrl} = this.state;
        if(!title){
            confirmAlert({
                //title: 'Error Message!',
                message: 'Card title  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!imgUrl){
            confirmAlert({
                //title: 'Error Message!',
                message: 'Image has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let data = {
            id : id,
            title : title,
            imgUrl : imgUrl || "",
            isActive : (isActive) ? 1 : 0,
            updatedtime : new Date().toISOString()
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    //title: 'Message Box!',
                    message: 'Save data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => this.setState({ error, submitted: false }));
        }
        
    }

    handleReset(e){
        this.componentDidMount();
    }
    render(){
        const {id,submitted,error,isActive,imgUrl,title} = this.state;
        //console.log(submitted);
        if(!id){
            return (<></>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                
                                    <CardHeader title="UPDATE CARD"></CardHeader>
                                   
                                    <CardContent>
                                        <Row>
                                            <Col md={12} lg={12}>
                                                <TextField required={true} style={{width:'100%',marginTop:10}} defaultValue={title} name="title" onChange={this.handleChange} label="TITLE" />
                                            </Col>
                                        </Row>
                                        <Row style={{paddingTop:15,paddingLeft:0}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIcon">
                                                    <label>IMAGE*</label>
                                                    <IconUpload 
                                                        ref={this.iconRef1}
                                                        defaultValue={imgUrl}
                                                        onSuccess={(image)=>{
                                                            this.setState({imgUrl:image.link});
                                                        }}
                                                    ></IconUpload>
                                                </FormGroup>
                                            </Col>
                                            
                                        </Row>
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={isActive}
                                                            onChange={this.handleChange}
                                                            name="isActive"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                </FormGroup>
                                                
                                            </Col>
                                        </Row>
                                    </CardContent>
                                    <CardActions style={{display:"block",textAlign:"right"}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        Discard
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        Save
                                        </Button>
                                    </CardActions>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;