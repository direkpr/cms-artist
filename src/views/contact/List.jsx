import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Icon from '@material-ui/core/Icon';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {authHeader,isAdmin} from '../../helpers'
import { FaEyeSlash,FaEye } from "react-icons/fa";
import * as moment from 'moment';
import {defaultService as service, reportService} from '../../services';
import { blue } from '@material-ui/core/colors';

import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Paper } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';
import { ExportToCsv } from 'export-to-csv';
import ContactList from './ContactList';

const _ = require('lodash');
const modelName ="albums";
class PageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subscription_type : -1,
            startDate :null,
            endDate : null,
            data : [],
            contacts : []
        }
        this.tableRef = React.createRef();
        this.ExportCSV = this.ExportCSV.bind(this);
        this.updateData = this.updateData.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e){
        const { name, value,checked } = e.target;
        //console.log([name,value]);
        this.setState({ [name]: value });
        setTimeout(this.updateData,300);
    }
    prepare_data_export(data){
        let results = [];
        _.each(data,(d)=>{
            
        })
        return data;
    }
    ExportCSV(){
        let {data} = this.state;
        //console.log(data);
        const options = { 
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true, 
            showTitle: false,
            title: 'CONTACT LIST',
            useTextFile: false,
            useBom: true,
            filename : "contact-"+moment().format("YYY-MM-DD"),
            useKeysAsHeaders: true,
            // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
        };
        const csvExporter = new ExportToCsv(options);
 
        if(data.length > 0){
            csvExporter.generateCsv(this.prepare_data_export(data));
        }
    }
    updateData(){
        let {startDate,endDate,contacts} = this.state;
        let data = contacts;
        //console.log([startDate,endDate]);
        if(!_.isNull(startDate)){
            let sDate = moment(startDate+" 00:00:00");
            data = _.filter(data,(o)=>{
                let cDate = moment(o.createdtime);
                return (cDate >= sDate);
            })
        }
        if(!_.isNull(endDate)){
            let eDate = moment(endDate+" 23:59:59");
            //console.log(eDate.toLocaleString());
            data = _.filter(data,(o)=>{
                let cDate = moment(o.createdtime);
                //console.log(cDate);
                return (cDate <= eDate);
            })
        }
        this.setState({data});
    }
    componentDidMount(){
        reportService.contact_list().then((data)=>{
            console.log(data);
            this.setState({contacts:data.data,data:data.data});
        })
    }
    render(){
        let {startDate,endDate,data} = this.state;
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                        <Paper variant="outlined" style={{padding:15,marginBottom:10}} >
                                <Typography component="h2">FILTER BY </Typography>
                                <Row>
                                    <Col md={5}>
                                    <TextField fullWidth
                                        id="startdate"
                                        label="Start Date"
                                        type="date"
                                        name="startDate"
                                        defaultValue={startDate}
                                        onChange={this.handleChange}
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                    </Col>
                                    <Col md={5}>

                                    <TextField fullWidth
                                        id="enddate"
                                        label="End Date"
                                        type="date"
                                        name="endDate"
                                        defaultValue={endDate}
                                        onChange={this.handleChange}
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                    />
                                    </Col>
                                    <Col md={2}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        startIcon={<SaveIcon />} 
                                        onClick={(e)=>{
                                            this.ExportCSV();
                                        }}
                                    >
                                        Export CSV
                                    </Button>
                                    </Col>
                                </Row>
                                
                            </Paper>

                            <ContactList data={data} {...this.props} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageList;

/*
<MaterialTable
                            title= "All Contacts"
                            tableRef = {this.tableRef}
                            
                            columns= {[
                                {
                                    title : 'Name',
                                    field : "fname",
                                    render : rowData=>{
                                        return rowData.fname+' '+rowData.lname;
                                    }
                                },
                                {
                                    title : 'Email',
                                    field : "email"
                                },
                                {
                                    title : 'Mobile',
                                    field : "mobile"
                                },
                                {
                                    title : 'Date',
                                    field : "createdtime",
                                    defaultSort : 'desc',
                                    width: 200,
                                    render : rowData=>{
                                        return moment(rowData.createdtime).format("YYYY-MM-DD hh:mm:ss")
                                        //return rowData.createdtime;
                                    }
                                }
                            ]}
                            data = {
                                query => new Promise((resolve,reject)=>{
                                    //console.log(query);
                                    let url = config.apiUrl+"/api/contact?"
                                        url += '&limit=' + query.pageSize
                                        url += '&offset=' + ((query.page * query.pageSize))
                                        url += '&search=' + query.search;
                                        if(!_.isUndefined(query.orderBy)){
                                            url += '&orderby='+query.orderBy.field+'&orderdirection='+query.orderDirection;
                                        }
                                        fetch(url,{
                                            method: 'GET',
                                            headers : authHeader()
                                        })
                                        .then(response => response.json())
                                        .then(result => {
                                            //console.log(result);
                                            resolve({
                                                data: result.data,
                                                page : result.page,
                                                totalCount : result.totalCount
                                            })
                                        }); 
                                    }
                                )
                            }
                            actions={[
                                {
                                    icon : 'visibility',
                                    tooltip: 'Edit',
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        return window.location = this.props.path+"/preview/"+rowData.id;
                                    }
                                },
                                rowData => ({
                                    icon: 'delete',
                                    tooltip : "Delete",
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        confirmAlert({
                                            title: 'Confirm Box!',
                                            message: 'You want to delete "'+rowData.fname+' '+rowData.lname+'"!',
                                            buttons: [
                                              {
                                                label: 'Yes',
                                                onClick: () => {
                                                    fetch(config.apiUrl+'/api/contact/'+rowData.id, {
                                                        method: 'DELETE',
                                                        headers : authHeader()
                                                    }).then((response) =>{
                                                        response.text().then(text =>{
                                                            this.tableRef.current && this.tableRef.current.onQueryChange()
                                                        });
                                                    })
                                                }
                                              },
                                              {
                                                label: 'No',
                                                onClick: () => {}
                                              }
                                            ]
                                        });
                                    }
                                })
                            ]}
                            options = {
                                {
                                    actionsColumnIndex: -1,
                                    search:true,
                                    paging:true,
                                    sorting:true
                                }
                            }
                            
                        />
*/