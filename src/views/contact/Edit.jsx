import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import {defaultService as service} from '../../services';
const _ = require('lodash');

const modelName ="contact";

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
}
function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box style={{paddingLeft:15,paddingRight:15}} p={3}>
            <Typography component="div">{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            isActive : true,
            coverUrl : undefined,
            year : undefined,
            lg : 0,
            langs : null,
            texts : []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddTags = this.handleAddTags.bind(this);
        this.handleDeleteTags = this.handleDeleteTags.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.tabsHandleChange = this.tabsHandleChange.bind(this);

        this.iconRef1 = React.createRef();
        this.iconRef2 = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "isActive"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleAddTags(tag){
        let {tags} = this.state;
        tags.push(tag);
        this.setState({tags});
    }
    handleDeleteTags(tag,index){
        let {tags} = this.state;
        tags = _.without(tags,tag);
        this.setState({tags});
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,year,submitted,isActive,coverUrl,texts,langs} = this.state;

        if(!year){
            confirmAlert({
                title: 'Error Message!',
                message: 'Album year  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let data = {
            id : id,
            year : year,
            coverUrl : coverUrl || "",
            isActive : isActive,
            texts : texts
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Save data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            //window.location = this.props.path+"/edit/"+response.id;
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => {
                this.setState({ error, submitted: false });
                confirmAlert({
                    title: 'Error Message!',
                    message: 'Save data fail!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                        }
                    }
                    ]
                });
            });
        }
        
    }

    handleReset(e){
        let {langs} = this.state;
        let texts = [];
        _.each(langs,(lang,key)=>{
            texts[key] = {lg:lang.code};
        })
        this.setState({isActive : true,
            imgUrl : undefined,
            link : undefined,
            texts:texts
        });
        this.iconRef1.current.onReset();
    }
    tabsHandleChange(event,value){
        this.setState({lg:value});
    }
    componentDidMount(){
        service.getById(this.props.match.params.id,modelName).then((data)=>{
            this.setState(data);
        })
    }
    render(){
        const {submitted,error,fname,lname,email,mobile,company,country,street1,street2,city,state,zipcode,subject,message,lg,id} = this.state;
        if(!id){
            return (<></>);
        }
        let address = street1 + ' '+street2;
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Container>
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                    <CardHeader title="CONTACT FORM DATA"></CardHeader>
                                    <CardContent>
                                        <Row>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                style={{width:'100%',marginTop:10}} defaultValue={fname} name="fname" label="FIRST NAME" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} defaultValue={lname} name="lname" label="LAST NAME" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                style={{width:'100%',marginTop:10}} defaultValue={email} name="email" label="EMAIL" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} defaultValue={mobile} name="mobile" label="MOBILE" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                style={{width:'100%',marginTop:10}} defaultValue={company} name="company" label="COMPANY" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} defaultValue={country} name="country" label="COUNTRY" />
                                            </Col>
                                            <Col md={12}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} style={{width:'100%',marginTop:10}} rows={5} multiline={true} defaultValue={address} name="street1" label="STREET ADDRESS" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                style={{width:'100%',marginTop:10}} defaultValue={city} name="city" label="CITY" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} defaultValue={state} name="state" label="STATE / PROVINCE / REGION" />
                                            </Col>
                                            <Col md={6}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} defaultValue={zipcode} name="zipcode" label="ZIPCODE" />
                                            </Col>
                                        </Row>
                                        <Row style={{marginTop:30}}>
                                            <Col md={12}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} 
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} defaultValue={subject} name="subject" label="SUBJECT" />
                                            </Col>
                                            <Col md={12}>
                                                <TextField InputProps={{
                                                    readOnly: true,
                                                }} style={{width:'100%',marginTop:10}} rows={5} multiline={true} defaultValue={message} name="message" label="MESSAGE" />
                                            </Col>
                                        </Row>
                                    </CardContent>
                                </Card>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;