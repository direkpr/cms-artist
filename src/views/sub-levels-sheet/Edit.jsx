import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import {sheetService as service,defaultService} from '../../services';
import Autocomplete from '@material-ui/lab/Autocomplete';
import SheetFileUpload from '../../components/SheetFileUpload/SheetFileUpload';
import VideoLink from '../../components/VideoLink/VideoLink';

const _ = require('lodash');


class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            is_active : true,
            imgUrl : undefined,
            title : "",
            tags : [],
            libraryLevelId : undefined,
            libraryLevel : null,
            librarySubLevel : null,
            librarySubLevelId : undefined,
            sublevels : [],
            videos : [{
                link : "",title:"",tags:[]
            }],
            sheetVideos : null,
            sheetFile : null,
            remark : ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddTags = this.handleAddTags.bind(this);
        this.handleDeleteTags = this.handleDeleteTags.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.handleChangeLevel = this.handleChangeLevel.bind(this);
        this.onUpdateVideos = this.onUpdateVideos.bind(this);
        this.videoRef = React.createRef();
        this.sheetfileRef = React.createRef();
        this.imageRef = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "is_active" || name === "is_comingsoon" || name == "only_subscriber"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleAddTags(tag){
        let {tags} = this.state;
        tags.push(tag);
        this.setState({tags});
    }
    handleDeleteTags(tag,index){
        let {tags} = this.state;
        tags = _.without(tags,tag);
        this.setState({tags});
    }
    componentDidMount(){
        service.getById(this.props.match.params.id).then(data=>{
            //console.log(data);
            data.videos = _.clone(data.sheetVideos);
            data.tags = data.tag.split(",") || [];
            defaultService.all({
                where : {is_active : true}
            },"library-levels").then((levels)=>{
                this.setState({levels,sublevels:[],librarySubLevel:null,librarySubLevelId:undefined});
                data.libraryLevel = _.find(levels,["id",data.libraryLevelId]);
                defaultService.all({
                    where : {is_active : true,libraryLevelId : data.libraryLevel.id}
                },"library-sub-levels").then((sublevels)=>{
                    this.setState({levels,sublevels});
                    data.librarySubLevel = _.find(sublevels,["id",data.librarySubLevelId]);
                    this.sheetfileRef.current.setFile(data.sheetFile);
                    //this.videoRef.current.setFiles(data.videos);
                    //console.log(data.videos);
                    this.setState(data);
                });
                
            });
            
        });

        
    }
    handleChangeLevel(libraryLevel){
        if(libraryLevel){
            defaultService.all({
                where : {is_active : true,libraryLevelId : libraryLevel.id}
            },"library-sub-levels").then((sublevels)=>{
                this.setState({sublevels:sublevels});
            });
        }
        this.setState({librarySubLevel : null,
            librarySubLevelId : undefined,sublevels:[]});
    }
    onUpdateVideos(videos){
       this.setState({videos});
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,title,tags,submitted,is_active,imgUrl,libraryLevel,librarySubLevel,videos,sheetFile,sheetVideos,remark} = this.state;
        if(!libraryLevel){
            confirmAlert({
                title: 'Error Message!',
                message: 'Level  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!librarySubLevel){
            confirmAlert({
                title: 'Error Message!',
                message: 'Sub Level  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!title){
            confirmAlert({
                title: 'Error Message!',
                message: 'Sheet title  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!imgUrl){
            confirmAlert({
                title: 'Error Message!',
                message: 'Sheet cover image has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        /*
        let validVideo = false;
        for(let i = 0; i < videos.length; i++){
            let video = videos[i];
            if(!_.isUndefined(video) && (video.title.length > 0) && (video.link.length > 0) && (video.tags.length > 0)){
                validVideo = true;
            }
        }
        if(!validVideo){
            confirmAlert({
                title: 'Error Message!',
                message: 'Video sheet has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }*/

        let datasearch = "";
        datasearch += title;
        datasearch += libraryLevel.title;
        datasearch += librarySubLevel.title;
        datasearch += tags.toString().replace(",",'');
        datasearch = datasearch.replace(/\s/g, '');
        datasearch = datasearch.toLocaleLowerCase();

        let data = {
            id : id,
            title : title,
            libraryLevelId : libraryLevel.id,
            librarySubLevelId : librarySubLevel.id,
            datasearch : datasearch,
            tag : tags.toString(),
            imgUrl : imgUrl,
            remark : remark || "",
            is_active : is_active,
            createdtime : new Date().toISOString(),
            updatedtime : new Date().toISOString()
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,sheetFile,[],[]).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Save data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            //window.location = this.props.path+"/edit/"+response.id;
                            //this.componentDidMount();
                            this.setState({ submitted: false });
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => this.setState({ error, submitted: false }))
        }
        
    }
    handleReset(e){
        this.componentDidMount();
        this.imageRef.current.onReset();
        this.sheetfileRef.current.onReset();
    }
    render(){
        let {id,submitted,error,is_active,imgUrl,tags,title,levels,libraryLevel,sublevels,librarySubLevel,videos,sheetFile,remark} = this.state;
        if(!levels){
            return (<></>);
        }
        //console.log(videos);
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                    <CardHeader title="EDIT LIBRARY SHEET"></CardHeader>
                                    <CardActions style={{display:"block",textAlign:"right"}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                    <CardContent>
                                        <Row>
                                            <Col  md={12} lg={12}>
                                            <Autocomplete
                                                options={levels} 
                                                onChange={(e,v)=>{
                                                    this.setState({libraryLevel:v});
                                                    this.handleChangeLevel(v);
                                                }}
                                                value={libraryLevel}
                                                getOptionLabel={(option) => option.title}
                                                style={{ width: '100%',marginTop:10 }}
                                                renderInput={(params) => <TextField {...params} label="Level" variant="outlined" />}
                                                />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col  md={12} lg={12}>
                                            <Autocomplete
                                                options={sublevels} 
                                                onChange={(e,v)=>{
                                                    this.setState({librarySubLevel:v});
                                                }}
                                                value={librarySubLevel}
                                                getOptionLabel={(option) => option.title}
                                                style={{ width: '100%',marginTop:15 }}
                                                renderInput={(params) => <TextField {...params} label="Sub Level" variant="outlined" />}
                                                />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={12} lg={12}>
                                                <TextField  style={{width:'100%',marginTop:10}} value={title} name="title" onChange={this.handleChange} label="TITLE" />
                                            </Col>
                                            <Col md={12}>
                                            <ChipInput
                                                label="TAGS" 
                                                style={{width:'100%',marginTop:10}}
                                                value={tags}
                                                onAdd={(chip) => this.handleAddTags(chip)}
                                                onDelete={(chip, index) => this.handleDeleteTags(chip, index)}
                                                />
                                            </Col>
                                        </Row>
                                        <Row style={{paddingTop:15}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIcon">
                                                    <label>IMAGE</label>
                                                    <IconUpload 
                                                    ref={this.imageRef}
                                                        defaultValue={imgUrl}
                                                        onSuccess={(image)=>{
                                                            this.setState({imgUrl:image.link});
                                                        }}
                                                    ></IconUpload>
                                                </FormGroup>
                                            </Col>
                                            <Col md={12}>
                                                <TextField
                                                    id="outlined-multiline-static"
                                                    label="Alt-Text Image"
                                                    style={{width:'100%',marginTop:20}}
                                                    multiline
                                                    rows={4}
                                                    value={remark} name="remark" onChange={this.handleChange}
                                                    variant="outlined"
                                                />
                                            </Col>
                                        </Row>
                                        <Row style={{paddingTop:15}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIcon">
                                                    <label>SHEET FILE</label>
                                                    <SheetFileUpload 
                                                        ref={this.sheetfileRef} 
                                                        defaultValue={sheetFile}
                                                        onSuccess={(pdf)=>{
                                                            this.setState({sheetFile:pdf});
                                                        }}
                                                    ></SheetFileUpload>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={is_active}
                                                            onChange={this.handleChange}
                                                            name="is_active"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </CardContent>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;

/*
<Row>
                                            <Col md={12}>
                                                <VideoLink ref={this.videoRef}  onUpdate={this.onUpdateVideos} />
                                            </Col>
                                        </Row>*/