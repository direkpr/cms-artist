import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import {sheetService as service,defaultService} from '../../services';
import Exercises from '../../components/Exercise/Exercise';
import {FaMinusCircle,FaPlusCircle} from "react-icons/fa";
import ExerciseHintImage from '../../components/ExerciseHintImage/ExerciseHintImage';

const _ = require('lodash');

class SheetExercice extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            exercises : [],
            sheet : null,
            librarySheetId : props.match.params.id || -1,
            hints : [],
            questions : []
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.refHintsImage = React.createRef();
        this.refQuestionsImage = React.createRef();
    }
    componentDidMount(){
        let librarySheetId = this.props.match.params.id;
        this.setState({librarySheetId : librarySheetId});
        let hints = [];
        let questions = [];
        service.getById(librarySheetId).then((sheet)=>{
            this.setState({sheet});
        })
        service.getSheetExercise(this.props.match.params.id).then(exercises=>{
            exercises = _.sortBy(exercises,["ex_no"]);
            if(_.isEmpty(exercises)){
                exercises = [{
                    librarySheetId : librarySheetId,
                    ex_no : 0,
                    answer : -1,
                    num_of_answer : 4,
                    have_hint : false,
                    hint_path : ""
                }];

            }else{
                _.each(exercises,(exercise,k)=>{
                    if(exercise.hint_path.length > 0){
                        hints.push({
                            name : exercise.ex_no,
                            img_url : exercise.hint_path
                        })
                    }
                });

                if(this.refHintsImage.current)
                    this.refHintsImage.current.setData(hints);

                _.each(exercises,(exercise,k)=>{
                    if(exercise.question_path.length > 0){
                        questions.push({
                            name : exercise.ex_no,
                            img_url : exercise.question_path
                        })
                    }
                });

                if(this.refQuestionsImage.current)
                    this.refQuestionsImage.current.setData(questions);
            }
            this.setState({exercises,hints,questions});
        });
    }
    
    handleSubmit(e) {
        e.preventDefault();
        const {submitted,librarySheetId,exercises,hints,questions} = this.state;
        _.each(exercises,(exercise)=>{
            exercise.hint_path = "";
            exercise.question_path = "";
        });
        _.each(hints,(hint)=>{
            let ex_no = parseInt(hint.name);
            let ex = _.find(exercises,["ex_no",ex_no]);
            if(ex){
                ex.hint_path = hint.img_url;
            }
        });
        _.each(questions,(question)=>{
            let ex_no = parseInt(question.name);
            let ex = _.find(exercises,["ex_no",ex_no]);
            if(ex){
                ex.question_path = question.img_url;
            }
        });
        //console.log(exercises);
        service.saveExercises(librarySheetId,exercises).then((response)=>{
            //console.log(response);
            confirmAlert({
                title: 'Message Box',
                message: 'Update data successful.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        //this.componentDidMount();
                        window.location = this.props.path+"/list";
                        return false;
                    }
                  }
                ]
            });
        });
    }
    handleReset(e){
        
    }
    render(){
        let {submitted,error,sheet,exercises,librarySheetId,hints,questions} = this.state;
        if(!sheet){
            return (<div></div>);
        }
        //console.log(hints);
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={10}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                    <CardHeader title="SHEET EXERCISE" subheader={sheet.title}></CardHeader>
                                    <CardActions style={{display:"block",textAlign:"right"}}>
                                        <Button size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                    <CardContent>
                                        <Row>
                                            <Col md={12}>
                                                {
                                                    (exercises) && exercises.map((exercise,key)=>(
                                                        <Exercises exercise={exercise} index={key} key={`exercise-key-${key}`} onUpdateAnswer={(v)=>{
                                                            exercises[key] = v;
                                                            this.setState(exercises);
                                                        }} onDelete={(v)=>{
                                                            delete exercises[key];
                                                            let temps = [];
                                                            for(let i= 0; i < exercises.length; i++){
                                                                if(!_.isEmpty(exercises[i])){
                                                                    temps.push(exercises[i]);
                                                                }
                                                            }
                                                            this.setState({exercises:temps});
                                                        }} />
                                                    ))
                                                }
                                            </Col>
                                        </Row>
                                        <Row style={{marginTop:30}}>
                                        <Col>
                                            <Button
                                                onClick={(e)=>{
                                                    let l = exercises.length;
                                                    exercises.push({
                                                        librarySheetId : librarySheetId,
                                                        ex_no : l,
                                                        answer : -1,
                                                        num_of_answer : 4,
                                                        have_hint : false,
                                                        hint_path : ""
                                                    });
                                                    this.setState({exercises});
                                                }}
                                                variant="contained"
                                                color="primary"
                                                size="small"
                                                startIcon={<FaPlusCircle />}
                                            >
                                                Add Exercise
                                            </Button>
                                        </Col>
                                    </Row>

                                    <Row style={{paddingTop:15}}>
                                        <Col md={6}>
                                            <FormGroup controlId="formControlsIcon">
                                            <label>Hint Images?</label>
                                            <ExerciseHintImage ref={this.refHintsImage} data={hints} onSuccess={(hints)=>{
                                                //console.log(hints);
                                                this.setState({hints});
                                            }} librarySheetId={librarySheetId} />
                                            </FormGroup>
                                        </Col>
                                        <Col md={6}>
                                            <FormGroup controlId="formControlsIcon">
                                            <label>Question Images?</label>
                                            <ExerciseHintImage ref={this.refQuestionsImage} data={questions} onSuccess={(questions)=>{
                                                this.setState({questions});
                                            }} librarySheetId={librarySheetId} />
                                            </FormGroup>
                                        </Col>
                                    </Row>

                                    <Row style={{paddingTop:15}}>
                                        
                                    </Row>

                                    </CardContent>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default SheetExercice;