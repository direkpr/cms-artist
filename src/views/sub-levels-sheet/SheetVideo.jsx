import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import {sheetService as service,defaultService} from '../../services';
import VideoLink from '../../components/VideoLink/VideoLink';
const _ = require('lodash');

class SheetVideo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            sheet : null,
            videos : null
        }
        this.onUpdateVideos = this.onUpdateVideos.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.videoRef = React.createRef();
    }
    componentDidMount(){
        let librarySheetId = this.props.match.params.id;
        this.setState({librarySheetId : librarySheetId});
        let hints = [];
        let questions = [];
        service.getById(librarySheetId).then((sheet)=>{
            let videos = _.clone(sheet.sheetVideos);
            //videos = _.sortBy(videos,["order_item asc"])
            //console.log(sheet);
            this.setState({sheet});
            this.setState({videos});
            this.videoRef.current.setFiles(videos);
        })
    }
    onUpdateVideos(videos){
        //console.log(videos);
        this.setState({videos});
    }
    handleSubmit(e) {
        e.preventDefault();
        let {sheet,submitted,videos} = this.state;
        
        let validVideo = false;
        for(let i = 0; i < videos.length; i++){
            let video = videos[i];
            if(!_.isUndefined(video) && (video.title.length > 0) && (video.link.length > 0) && (video.tags.length > 0)){
                validVideo = true;
            }
        }
        if(!validVideo){
            confirmAlert({
                title: 'Error Message!',
                message: 'Video sheet has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }

        let data = {
            id : sheet.id,
            updatedtime : new Date().toISOString()
        }
        this.setState({ submitted: true });
        service.update(data,sheet.sheetFile,videos,sheet.sheetVideos).then(response=>{
            confirmAlert({
                title: 'Message Box!',
                message: 'Save data successful!',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        //window.location = this.props.path+"/edit/"+response.id;
                        //this.componentDidMount();
                        window.location = this.props.path+"/list";
                    }
                  }
                ]
            });
        },error => this.setState({ error, submitted: false }))
    }
    handleReset(e){
        this.componentDidMount();
    }
    render(){
        let {submitted,error,sheet} = this.state;
        if(!sheet){
            return (<div></div>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={10}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                    <CardHeader title="SHEET VIDEO" subheader={sheet.title}></CardHeader>
                                    <CardActions style={{display:"block",textAlign:"right"}}>
                                        <Button size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                    <CardContent>
                                        <Row>
                                            <Col md={12}>
                                                <VideoLink ref={this.videoRef}  onUpdate={this.onUpdateVideos} />
                                            </Col>
                                        </Row>
                                    </CardContent>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default SheetVideo;
