import React from 'react';
import { Route,Switch } from "react-router-dom";
import PageList from './List';
import PageAdd from './Add';
import PageEdit from './Edit';
import SheetExercice from './SheetExercice'
import SheetVideo from './SheetVideo'
class Sheets extends React.Component {
    
    render(){
        return (
            <div className="content">
                <Switch>
                    <Route path={this.props.path+"/add"}  
                        render={props => (
                            <PageAdd path={this.props.path}
                            {...props}
                            showNotification = {this.props.showNotification}
                            />
                        )}
                    />
                    <Route path={this.props.path+"/edit/:id"} 
                        render={props => (
                            <PageEdit path={this.props.path}
                            {...props}
                            showNotification = {this.props.showNotification}
                            />
                        )}
                    />
                    <Route path={this.props.path+"/sheet-exercice/:id"} 
                        render={props => (
                            <SheetExercice path={this.props.path}
                            {...props}
                            showNotification = {this.props.showNotification}
                            />
                        )}
                    />
                    <Route path={this.props.path+"/sheet-video/:id"} 
                        render={props => (
                            <SheetVideo path={this.props.path}
                            {...props}
                            showNotification = {this.props.showNotification}
                            />
                        )}
                    />
                    <Route path={this.props.path}
                        render={props => (
                            <PageList path={this.props.path}
                            {...props}
                            showNotification = {this.props.showNotification}
                            />
                        )} 
                    />
                </Switch>
            </div>
        )
    }
}
export default Sheets;