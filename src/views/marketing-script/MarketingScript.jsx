import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import {defaultService as service} from '../../services';
import Autocomplete from '@material-ui/lab/Autocomplete';

const _ = require('lodash');

const modelName ="marketingscripts";
class MarketingScript extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            in_head_tag:undefined,
            open_body_tag:undefined,
            close_body_tag:undefined
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        this.setState({ [name]: value });
    }
    
    componentDidMount(){
        service.getById(1,modelName).then(data=>{
            this.setState(data);
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,submitted,in_head_tag,open_body_tag,close_body_tag} = this.state;
        
        let data = {
            id : id,
            in_head_tag:in_head_tag,
            open_body_tag:open_body_tag,
            close_body_tag:close_body_tag,
            updatedtime : new Date().toISOString()
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Update data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.componentDidMount();
                            this.setState({ submitted: false });
                        }
                    }
                    ]
                });
            },error => this.setState({ error, submitted: false }))
        }
        
    }

    handleReset(e){
        e.preventDefault();
        this.componentDidMount();
        //this.imageRef.current.onReset();
    }
    render(){
        let {id,submitted,error,in_head_tag,open_body_tag,close_body_tag} = this.state;
        if(!id){
            return (<></>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                    <CardHeader title="Update Marketing Scripts"></CardHeader>
                                    <CardContent>
                                        <Row>
                                            <Col md={12} lg={12}>
                                                <TextField InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} multiline={true} rows={5} value={in_head_tag} required={true} name="in_head_tag" onChange={this.handleChange} label="In <head> tag" />
                                            </Col>
                                            <Col md={12} lg={12}>
                                                <TextField InputLabelProps={{
                                                    shrink: true,
                                                }} style={{width:'100%',marginTop:10}} multiline={true} rows={5} value={open_body_tag} required={true} name="open_body_tag" onChange={this.handleChange} label="After open <body> tag" />
                                            </Col>
                                            <Col md={12} lg={12}>
                                                <TextField InputLabelProps={{
                                                    shrink: true,
                                                }}  style={{width:'100%',marginTop:10}} multiline={true} rows={5} value={close_body_tag} required={true} name="close_body_tag" onChange={this.handleChange} label="Before close <body> tag" />
                                            </Col>
                                        </Row>
                                        
                                    </CardContent>
                                    <CardActions style={{padding:15}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default MarketingScript;