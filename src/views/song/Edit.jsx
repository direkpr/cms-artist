import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import {defaultService as service} from '../../services';
const _ = require('lodash');

const modelName ="songs";

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
}
function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box style={{paddingLeft:15,paddingRight:15}} p={3}>
            <Typography component="div">{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            isActive : true,
            album_id : "",
            stream_preview : undefined,
            stream_full : undefined,
            link : undefined,
            duration : undefined,
            lg : 0,
            langs : null,
            texts : []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddTags = this.handleAddTags.bind(this);
        this.handleDeleteTags = this.handleDeleteTags.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.tabsHandleChange = this.tabsHandleChange.bind(this);

        this.iconRef1 = React.createRef();
        this.iconRef2 = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        //console.log([name,value]);
        //console.log(e.target);
        if(name === "isActive"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleAddTags(tag){
        let {tags} = this.state;
        tags.push(tag);
        this.setState({tags});
    }
    handleDeleteTags(tag,index){
        let {tags} = this.state;
        tags = _.without(tags,tag);
        this.setState({tags});
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,submitted,isActive,album_id,stream_preview,stream_full,link,duration,texts,langs} = this.state;

        if(!album_id){
            confirmAlert({
                title: 'Error Message!',
                message: 'Album  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!stream_preview){
            confirmAlert({
                title: 'Error Message!',
                message: 'URL demo preview has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }

        if(!stream_full){
            confirmAlert({
                title: 'Error Message!',
                message: 'URL full preview has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }

        if(!link){
            confirmAlert({
                title: 'Error Message!',
                message: 'URL purchase has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }

        if(!duration){
            confirmAlert({
                title: 'Error Message!',
                message: 'Song duration has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }

        let data = {
            id : id,
            album_id : album_id,
            stream_preview : stream_preview,
            stream_full : stream_full,
            link : link,
            duration : duration,
            isActive : isActive,
            texts : texts
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Save data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            //window.location = this.props.path+"/edit/"+response.id;
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => {
                this.setState({ error, submitted: false });
                confirmAlert({
                    title: 'Error Message!',
                    message: 'Save data fail!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                        }
                    }
                    ]
                });
            });
        }
        
    }

    handleReset(e){
        let {langs} = this.state;
        let texts = [];
        _.each(langs,(lang,key)=>{
            texts[key] = {lg:lang.code};
        })
        this.setState({isActive : true,
            album_id : undefined,
            stream_preview : undefined,
            stream_full : undefined,
            link : undefined,
            duration : undefined,
            texts:texts
        });
        this.componentDidMount();
    }
    tabsHandleChange(event,value){
        this.setState({lg:value});
    }
    componentDidMount(){
        service.get_langs().then(langs=>{
            //console.log(langs);
            let texts = [];
            _.each(langs,(lang,key)=>{
                texts[key] = {lg:lang.code};
            })
            this.setState({langs,texts});
            service.all("albums","limit=100&offset=0").then((albums)=>{
                //console.log(albums);
                this.setState({albums});
                service.getById(this.props.match.params.id,modelName).then((data)=>{
                    _.each(langs,(lang,key)=>{
                        let idx = _.find(data.texts,["lg",lang.code]);
                        if(_.isUndefined(idx)){
                            data.texts.push({lg:lang.code})
                        }
                    })
                    data.isActive = (data.isActive == 1) ? true : false;
                    this.setState(data);
                })
            })
        });
        
    }
    render(){
        const {id,submitted,error,isActive,album_id,stream_preview,stream_full,link,duration,lg,langs,texts,albums} = this.state;
        if(!langs || !albums || !id){
            return (<></>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                
                                    <CardHeader title="UPDATE SONG"></CardHeader>
                                    
                                    <CardContent>
                                        <Paper square>
                                        <Tabs
                                            value={lg}
                                            indicatorColor="primary"
                                            textColor="primary"
                                            variant="fullWidth"
                                            onChange={this.tabsHandleChange}
                                        >
                                            {
                                                (langs) && langs.map((lang,key)=>(
                                                    <Tab key={key} label={lang.title} {...a11yProps(key)} />
                                                ))
                                            }
                                            
                                        </Tabs>
                                        {
                                            (langs) && langs.map((lang,key)=>(
                                                <TabPanel key={key} value={lg} index={key}>
                                                    <Row>
                                                        <Col md={12} lg={12}>
                                                            <TextField required={(lang.isDefault == 1)} style={{width:'100%',marginTop:10}} defaultValue={texts[key].name} name="name" onChange={(e)=>{
                                                                texts[key].name = e.target.value;
                                                                this.setState({texts});
                                                            }} label="NAME" />
                                                        </Col>
                                                        <Col md={12} lg={12}>
                                                            <TextField required={(lang.isDefault == 1)} style={{width:'100%',marginTop:10}} defaultValue={texts[key].artist} name="artist" onChange={(e)=>{
                                                                texts[key].artist = e.target.value;
                                                                this.setState({texts});
                                                            }} label="ARTIST" />
                                                        </Col>
                                                        <Col md={12} lg={12}>
                                                            <TextField required={(lang.isDefault == 1)} style={{width:'100%',marginTop:10}} defaultValue={texts[key].price} name="price" onChange={(e)=>{
                                                                texts[key].price = e.target.value;
                                                                this.setState({texts});
                                                            }} helperText="Price (include tax)" label="PRICE" />
                                                        </Col>
                                                    </Row>
                                                </TabPanel>
                                            ))
                                        }

                                            <Row style={{padding:15}}>
                                                <Col md={12} lg={12}>
                                                    <FormControl style={{width:'100%',marginTop:10}}>
                                                        <InputLabel>ALBUM *</InputLabel>
                                                        <Select required={true}
                                                        name="album_id"
                                                        value={album_id}
                                                        onChange={this.handleChange}
                                                        >
                                                        <MenuItem value="">
                                                            <em>None</em>
                                                        </MenuItem>
                                                        {
                                                            albums && albums.data.map((album,key)=>(
                                                                <MenuItem key={key} value={album.id}>{album.title}</MenuItem>
                                                            ))
                                                        }
                                                        </Select>
                                                    </FormControl>
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField
                                                        helperText="URL for preivew demo song (30 sec.)"
                                                        required={true} style={{width:'100%',marginTop:10}} defaultValue={stream_preview} name="stream_preview" onChange={this.handleChange} label="PRVIVIEW URL" />
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField 
                                                        helperText="URL for preivew full song"
                                                        required={true} style={{width:'100%',marginTop:10}} defaultValue={stream_full} name="stream_full" onChange={this.handleChange} label="FULL URL" />
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField 
                                                        helperText="Link to BASE app for purchase this song"
                                                        required={true} style={{width:'100%',marginTop:10}} defaultValue={link} name="link" onChange={this.handleChange} label="LINK" />
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField 
                                                        helperText="Song duration in `Second`"
                                                        type="number" required={true} style={{width:'100%',marginTop:10}} defaultValue={duration} name="duration" onChange={this.handleChange} label="DURATION" />
                                                </Col>
                                            </Row>
                                         </Paper>


                                        
                                        
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={isActive}
                                                            onChange={this.handleChange}
                                                            name="isActive"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                </FormGroup>
                                                
                                            </Col>
                                        </Row>
                                    </CardContent>
                                    <CardActions style={{padding:15}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;