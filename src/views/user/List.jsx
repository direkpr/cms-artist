import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Icon from '@material-ui/core/Icon';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {authHeader,isAdmin,isMyAccount} from '../../helpers'
import { FaEyeSlash,FaEye } from "react-icons/fa";
import * as moment from 'moment';
import {defaultService as service} from '../../services';
import { blue } from '@material-ui/core/colors';
const _ = require('lodash');
class PageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
        this.tableRef = React.createRef();
    }
    componentDidMount(){
    }
    render(){
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                        <MaterialTable
                            title= "All User"
                            tableRef = {this.tableRef}
                            columns= {[
                                {
                                    title : 'Name',
                                    field : "firstname",
                                    render: rowData=>{
                                        return rowData.firstname+" "+rowData.lastname;
                                    }
                                },
                                {
                                    title : "Email",
                                    field : "email"
                                },
                                {
                                    title : "Roles",
                                    field : "roles",
                                    render : rowData=>{
                                        if(rowData.roles){
                                            let textroles = {"admin":"Admin","manager":"Manage Content","report":"Report"};

                                            return rowData.roles.map((role)=>{ return textroles[role];}).join();
                                        }
                                        else
                                            return "";
                                    }
                                },
                                {
                                    title : 'Date',
                                    field : "createdtime",
                                    width: 200,
                                    render : rowData=>{
                                        return moment(rowData.createdtime).format("YYYY-MM-DD hh:mm:ss")
                                    }
                                }
                            ]}
                            data = {
                                query => new Promise((resolve,reject)=>{
                                    let url = config.apiUrl+"/api/admin/members?"
                                        url += '&limit=' + query.pageSize
                                        url += '&offset=' + ((query.page * query.pageSize))
                                        url += '&search=' + query.search;
                                        if(!_.isUndefined(query.orderBy)){
                                            url += '&orderby='+query.orderBy.field+'&orderdirection='+query.orderDirection;
                                        }
                                        fetch(url,{
                                            method: 'GET',
                                            headers : authHeader()
                                        })
                                        .then(response => response.json())
                                        .then(result => {
                                            //console.log(result);
                                            _.each(result.data,(r)=>{
                                                r.roles = JSON.parse(r.roles);
                                            });
                                            resolve({
                                                data: result.data,
                                                page : result.page,
                                                totalCount : result.totalCount
                                            })
                                        }); 
                                    }
                                )
                            }
                            actions={[
                                {
                                  icon: 'add_circle',
                                  tooltip: 'Add New',
                                  isFreeAction: true,
                                  disabled:!isAdmin() ,
                                  onClick: () => {
                                      return window.location = this.props.path+"/add";
                                  },
                                },
                                {
                                    icon : 'edit',
                                    tooltip: 'Edit',
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        return window.location = this.props.path+"/edit/"+rowData.id;
                                    }
                                },
                                rowData => ({
                                    icon: 'delete',
                                    tooltip : "Delete",
                                    disabled:!isAdmin() || isMyAccount(rowData.id),
                                    onClick : (event,rowData) =>{
                                        confirmAlert({
                                            title: 'Confirm Box!',
                                            message: 'You want to delete "'+rowData.firstname+' '+rowData.lastname+'"!',
                                            buttons: [
                                              {
                                                label: 'Yes',
                                                onClick: () => {
                                                    fetch(config.apiUrl+'/api/admin/members/'+rowData.id, {
                                                        method: 'DELETE',
                                                        headers : authHeader()
                                                    }).then((response) =>{
                                                        response.text().then(text =>{
                                                            this.tableRef.current && this.tableRef.current.onQueryChange()
                                                        });
                                                    })
                                                }
                                              },
                                              {
                                                label: 'No',
                                                onClick: () => {}
                                              }
                                            ]
                                        });
                                    }
                                })
                            ]}
                            options = {
                                {
                                    actionsColumnIndex: -1,
                                    search:true,
                                    paging:true,
                                    sorting:true
                                }
                            }
                            
                        />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageList;