import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent, FormLabel } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import {userService as service,defaultService} from '../../services';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
const _ = require('lodash');

const modelName ="users";
class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            firstname: undefined,
            lastname : undefined,
            email : undefined,
            password : undefined,
            cpassword : undefined,
            roles : []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,submitted,lastname,firstname,email,password,cpassword,roles} = this.state;
        if(!firstname){
            confirmAlert({
                title: 'Error Message!',
                message: 'firstname  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!lastname){
            confirmAlert({
                title: 'Error Message!',
                message: 'lastname  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let newPassword = "USEOLDUSEOLD";
        if(password && password.length > 0){
            if(password.length < 8){
                confirmAlert({
                    title: 'Error Message!',
                    message: 'Password must be at least 8 characters',
                    buttons: [
                      {
                        label: 'OK',
                        onClick: () => {
                            return false;
                        }
                      }
                    ]
                });
                return false;
            }
            if(password !== cpassword){
                confirmAlert({
                    title: 'Error Message!',
                    message: 'Password & Confirm password not match',
                    buttons: [
                      {
                        label: 'OK',
                        onClick: () => {
                            return false;
                        }
                      }
                    ]
                });
                return false;
            }
            newPassword = password;
        }
        let data = {
            id : id,
            firstname : firstname,
            lastname : lastname || "",
            email : email || "",
            password : newPassword,
            roles : roles,
            use_cms : 1,
            payment_status : 1,
            updatedtime : new Date().toISOString()
        }

        if(!submitted){
            this.setState({ submitted: true });
            service.updateUser(data).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Update data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => {
                this.setState({ error, submitted: false })
                confirmAlert({
                    title: 'Error message!',
                    message: "Update data fail",
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                        }
                    }
                    ]
                });
            });
        }
        
    }

    handleReset(e){
        this.componentDidMount();
    }
    componentDidMount(){
        service.getById(this.props.match.params.id).then(data=>{
            //console.log(data);
            this.setState(data);
        })
    }
    render(){
        let {id,submitted,error,lastname,firstname,email,password,cpassword,roles} = this.state;
        if(!id){
            return (<></>);
        }
        let iAdmin = (_.indexOf(roles,"admin") > -1);
        let iManager = (_.indexOf(roles,"manager") > -1);
        let iReport = (_.indexOf(roles,"report") > -1);
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                
                                    <CardHeader title="UPDATE USER"></CardHeader>
                                   
                                    <CardContent>
                                        <Row>
                                            <Col md={6} lg={6}>
                                                <TextField required={true} style={{width:'100%',marginTop:10}} value={firstname} name="firstname" onChange={this.handleChange} label="FIRST NAME" />
                                            </Col>
                                            <Col md={6} lg={6}>
                                                <TextField required={true} style={{width:'100%',marginTop:10}} value={lastname} name="lastname" onChange={this.handleChange} label="LAST NAME" />
                                            </Col>
                                            <Col md={12} lg={12}>
                                                <TextField required={true} disabled={true} style={{width:'100%',marginTop:10}} type="email" value={email} name="email" onChange={this.handleChange} label="EMAIL (USERNAME)" />
                                            </Col>
                                            <Col md={6} lg={6}>
                                                <TextField type="password"  style={{width:'100%',marginTop:10}} value={password} name="password" onChange={this.handleChange} label="PASSWORD" />
                                            </Col>
                                            <Col md={6} lg={6}>
                                                <TextField type="password"  style={{width:'100%',marginTop:10}} value={cpassword} name="cpassword" onChange={this.handleChange} label="CONFIRM PASSWORD" />
                                            </Col>

                                            
                                        </Row>
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12} lg={12}>
                                            <FormLabel>ROLE</FormLabel>
                                            <FormGroup >
                                                <FormControlLabel
                                                    control={<Checkbox checked={iAdmin} onChange={(e,v)=>{
                                                        roles = ["admin"];
                                                        this.setState({roles});
                                                    }} name="admin"  />}
                                                    label="Admin"
                                                />
                                                
                                                <FormControlLabel
                                                    control={<Checkbox checked={iManager} onChange={(e,v)=>{
                                                        roles = ["manager"];
                                                        this.setState({roles});
                                                    }} name="manager"  />}
                                                    label="Manage Content"
                                                />
                                                <FormControlLabel
                                                    control={<Checkbox checked={iReport} onChange={(e,v)=>{
                                                        roles = ["report"];
                                                        this.setState({roles});
                                                    }} name="report" />}
                                                    label="Reports"
                                                />
                                            </FormGroup>
                                            </Col>
                                        </Row>
                                        
                                    </CardContent>
                                    <CardActions style={{display:"block"}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted || roles.length == 0} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                </Card>
                                <Row>
                                            <Col>
                                            <p style={{marginBottom:0,marginTop:10}}><b>Level Roles :</b></p>
                                            <ul>
                                                <li><b>Reports</b> - Able to access Reports menu</li>
                                                <li><b>Manage Content</b> - Able to access menus that require content management</li>
                                                <li><b>Admin</b> - Able to access all menus</li>
                                            </ul>
                                            </Col>
                                        </Row>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;