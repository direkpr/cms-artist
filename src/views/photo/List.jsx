import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Icon from '@material-ui/core/Icon';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {authHeader,isAdmin} from '../../helpers'
import { FaEyeSlash,FaEye } from "react-icons/fa";
import * as moment from 'moment';
import {defaultService as service} from '../../services';
import { blue } from '@material-ui/core/colors';
const _ = require('lodash');
const modelName ="photo";

class PageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
        this.tableRef = React.createRef();
    }
    render(){
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                        <MaterialTable
                            title= "All Photo"
                            tableRef = {this.tableRef}
                            
                            columns= {[
                                {
                                    title : "Preview",
                                    field : 'id',
                                    width : 100,
                                    render: rowData =>{
                                        return <img src={config.imgUrl+rowData.thumUrl} width="100" />
                                    }
                                },
                                {
                                    title : 'Title',
                                    field : "name",
                                },
                                {
                                    title : 'Price',
                                    field : "price",
                                },
                                {
                                    title : 'Created Date',
                                    field : "createdtime",
                                    width: 200,
                                    render : rowData=>{
                                        return moment(rowData.createdtime).format("YYYY-MM-DD hh:mm:ss")
                                    }
                                },
                                {
                                    title : 'Updated Date',
                                    field : "updatedtime",
                                    defaultSort : 'desc',
                                    width: 200,
                                    render : rowData=>{
                                        return moment(rowData.updatedtime).format("YYYY-MM-DD hh:mm:ss")
                                    }
                                }
                            ]}
                            data = {
                                query => new Promise((resolve,reject)=>{
                                    //console.log(query);
                                    let url = config.apiUrl+"/api/"+modelName+"?"
                                        url += '&limit=' + query.pageSize
                                        url += '&offset=' + ((query.page * query.pageSize))
                                        url += '&search=' + query.search;
                                        if(!_.isUndefined(query.orderBy)){
                                            url += '&orderby='+query.orderBy.field+'&orderdirection='+query.orderDirection;
                                        }
                                        fetch(url,{
                                            method: 'GET',
                                            headers : authHeader()
                                        })
                                        .then(response => response.json())
                                        .then(result => {
                                            //console.log(result);
                                            resolve({
                                                data: result.data,
                                                page : result.page,
                                                totalCount : result.totalCount
                                            })
                                        }); 
                                    }
                                )
                            }
                            actions={[
                                {
                                  icon: 'add_circle',
                                  iconProps:{style:{backgroundColor:'transparent !important'}},
                                  tooltip: 'Add New',
                                  isFreeAction: true,
                                  disabled:!isAdmin(),
                                  onClick: () => {
                                      return window.location = this.props.path+"/add";
                                  },
                                },
                                rowData => ({
                                    icon: (rowData.isActive == 1) ? "visibility" : "visibility_off",
                                    tooltip : (rowData.isActive == 1) ? "Active" : "Not Active",
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        service.setActive(rowData.id,!(rowData.isActive == 1),modelName).then(
                                            (data)=>{
                                                this.tableRef.current && this.tableRef.current.onQueryChange();
                                            }
                                        );
                                    }
                                }),
                                {
                                    icon : 'edit',
                                    tooltip: 'Edit',
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        return window.location = this.props.path+"/edit/"+rowData.id;
                                    }
                                },
                                rowData => ({
                                    icon: 'delete',
                                    tooltip : "Delete",
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        confirmAlert({
                                            title: 'Confirm Box!',
                                            message: 'You want to delete "'+rowData.good_name+'"!',
                                            buttons: [
                                              {
                                                label: 'Yes',
                                                onClick: () => {
                                                    fetch(config.apiUrl+'/api/'+modelName+'/'+rowData.id, {
                                                        method: 'DELETE',
                                                        headers : authHeader()
                                                    }).then((response) =>{
                                                        response.text().then(text =>{
                                                            this.tableRef.current && this.tableRef.current.onQueryChange()
                                                        });
                                                    })
                                                }
                                              },
                                              {
                                                label: 'No',
                                                onClick: () => {}
                                              }
                                            ]
                                        });
                                    }
                                })
                            ]}
                            options = {
                                {
                                    actionsColumnIndex: -1,
                                    search:true,
                                    paging:true,
                                    sorting:true
                                }
                            }
                            
                        />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageList;