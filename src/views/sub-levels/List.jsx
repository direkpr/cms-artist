import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {authHeader,isAdmin} from '../../helpers'
import {defaultService as service} from '../../services';
import * as moment from 'moment';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'

const modelName ="library-sub-levels";
const _ = require('lodash');

class PageList extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
        this.tableRef = React.createRef();
    }
    render(){
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                        <MaterialTable
                            title= "All Sub Levels"
                            tableRef = {this.tableRef}
                            columns= {[
                                {
                                    title : 'Title',
                                    field : "title",
                                },
                                {
                                    title : 'Level',
                                    field : "libraryLevel.title"
                                },
                                {
                                    title : 'Date',
                                    field : "createdtime",
                                    width: 200,
                                    render : rowData=>{
                                        return moment(rowData.createdtime).format("YYYY-MM-DD hh:mm:ss")
                                    }
                                },
                                {
                                    title : "Coming Soon",
                                    field : 'is_comingsoon',
                                    width: 150,
                                    render : rowData=><span>{(rowData.is_comingsoon) ? "Yes":"No"}</span>
                                }
                            ]}
                            data = {
                                query => new Promise((resolve,reject)=>{
                                    let url = config.apiUrl+"/admin/librarysublevels?"
                                        url += '&limit=' + query.pageSize
                                        url += '&offset=' + ((query.page * query.pageSize))
                                        url += '&search=' + query.search
                                        fetch(url,{
                                            method: 'GET',
                                            headers : authHeader()
                                        })
                                        .then(response => response.json())
                                        .then(result => {
                                            resolve({
                                                data: result.data,
                                                page : result.page,
                                                totalCount : result.totalCount
                                            })
                                        }); 
                                    }
                                )
                            }
                            actions={[
                                {
                                    icon: (props)=>(
                                        <Button {...props}
                                            component="div"
                                            variant="contained"
                                            startIcon={<AddCircleIcon />}
                                        >
                                        ADD
                                      </Button>
                                      ),
                                  tooltip: 'Add New',
                                  isFreeAction: true,
                                  disabled:!isAdmin(),
                                  onClick: () => {
                                      return window.location = this.props.path+"/add";
                                  },
                                },
                                rowData => ({
                                    icon: (rowData.is_active) ? "visibility" : "visibility_off",
                                    iconProps: {style:{color:'rgb(45 126 240)'}},
                                    tooltip : (rowData.is_active) ? "Active" : "Not Active",
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        service.setActive(rowData.id,!rowData.is_active,modelName).then(
                                            (data)=>{
                                                this.tableRef.current && this.tableRef.current.onQueryChange();
                                            }
                                        );
                                    }
                                }),
                                {
                                    icon : 'edit',
                                    iconProps: {style:{color:'rgb(70 148 70)'}},
                                    tooltip: 'Edit',
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        return window.location = this.props.path+"/edit/"+rowData.id;
                                    }
                                },
                                rowData => ({
                                    icon: 'delete',
                                    iconProps: {style:{color:'#f00'}},
                                    tooltip : "Delete",
                                    disabled:!isAdmin(),
                                    onClick : (event,rowData) =>{
                                        confirmAlert({
                                            title: 'Confirm Box!',
                                            message: 'You want to delete "'+rowData.title+'"!',
                                            buttons: [
                                              {
                                                label: 'Yes',
                                                onClick: () => {
                                                    fetch(config.apiUrl+'/library-sub-levels/'+rowData.id, {
                                                        method: 'DELETE',
                                                        headers : authHeader()
                                                    }).then((response) =>{
                                                        response.text().then(text =>{
                                                            this.tableRef.current && this.tableRef.current.onQueryChange()
                                                        });
                                                    })
                                                }
                                              },
                                              {
                                                label: 'No',
                                                onClick: () => {}
                                              }
                                            ]
                                        });
                                    }
                                })
                            ]}
                            options = {
                                {
                                    actionsColumnIndex: -1,
                                    search:true,
                                    paging:true,
                                    sorting:true
                                }
                            }
                        />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageList;