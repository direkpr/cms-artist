import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import {defaultService as service} from '../../services';
import Autocomplete from '@material-ui/lab/Autocomplete';

const _ = require('lodash');

const modelName ="library-sub-levels";
class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            is_active : true,
            is_comingsoon : false,
            only_subscriber : true,
            imgUrl : undefined,
            title : undefined,
            remark : undefined,
            tags : [],
            libraryLevelId : undefined,
            libraryLevel : null
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddTags = this.handleAddTags.bind(this);
        this.handleDeleteTags = this.handleDeleteTags.bind(this);
        this.handleReset = this.handleReset.bind(this);

        this.imageRef = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "is_active" || name === "is_comingsoon" || name == "only_subscriber"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleAddTags(tag){
        let {tags} = this.state;
        tags.push(tag);
        this.setState({tags});
    }
    handleDeleteTags(tag,index){
        let {tags} = this.state;
        tags = _.without(tags,tag);
        this.setState({tags});
    }
    componentDidMount(){
        service.all({
            where : {is_active : true}
        },"library-levels").then((levels)=>{
            this.setState({levels});

            service.getById(this.props.match.params.id,modelName).then(data=>{
                //console.log(data);
                data.tags = data.tag.split(",") || [];
                data.libraryLevel = _.find(levels,["id",data.libraryLevelId]);
                this.setState(data);
            });
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,title,tags,remark,submitted,is_active,is_comingsoon,imgUrl,only_subscriber,libraryLevel} = this.state;
        if(!libraryLevel){
            confirmAlert({
                title: 'Error Message!',
                message: 'Level  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!title){
            confirmAlert({
                title: 'Error Message!',
                message: 'Sub Level title  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!imgUrl){
            confirmAlert({
                title: 'Error Message!',
                message: 'Sub Level cover image has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let datasearch = "";
        datasearch += title;
        datasearch += libraryLevel.title;
        datasearch += tags.toString().replace(",",'');
        datasearch = datasearch.replace(/\s/g, '');
        datasearch = datasearch.toLocaleLowerCase();

        let data = {
            id : id,
            title : title,
            libraryLevelId : libraryLevel.id,
            datasearch : datasearch,
            remark : remark || "",
            tag : tags.toString(),
            imgUrl : imgUrl,
            is_active : is_active,
            is_comingsoon : is_comingsoon,
            only_subscriber : only_subscriber,
            createdtime : new Date().toISOString(),
            updatedtime : new Date().toISOString()
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Update data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            //this.componentDidMount();
                            this.setState({ submitted: false });
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => this.setState({ error, submitted: false }))
        }
        
    }

    handleReset(e){
        e.preventDefault();
        this.componentDidMount();
        this.imageRef.current.onReset();
    }
    render(){
        let {id,submitted,error,is_active,is_comingsoon,only_subscriber,imgUrl,tags,title,remark,levels,libraryLevel} = this.state;
        if(!levels || !id){
            return (<></>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                    <CardHeader title="UPDATE LIBRARY SUBLEVEL"></CardHeader>
                                    <CardActions style={{display:"block",textAlign:"right"}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                    <CardContent>
                                        <Row>
                                            <Col  md={12} lg={12}>
                                            <Autocomplete
                                                options={levels} 
                                                onChange={(e,v)=>{
                                                    this.setState({libraryLevel:v});
                                                }}
                                                value={libraryLevel}
                                                getOptionLabel={(option) => option.title}
                                                style={{ width: '100%' }}
                                                renderInput={(params) => <TextField {...params} required={true} label="Level" variant="outlined" />}
                                                />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={12} lg={12}>
                                                <TextField InputLabelProps={{
                                                    shrink: true,
                                                }}  style={{width:'100%',marginTop:10}} value={title} required={true} name="title" onChange={this.handleChange} label="TITLE" />
                                            </Col>
                                            
                                            <Col md={12}>
                                            <ChipInput
                                                label="TAGS" 
                                                style={{width:'100%',marginTop:10}}
                                                value={tags}
                                                onAdd={(chip) => this.handleAddTags(chip)}
                                                onDelete={(chip, index) => this.handleDeleteTags(chip, index)}
                                                />
                                            </Col>
                                            
                                        </Row>
                                        <Row style={{paddingTop:15}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIcon">
                                                    <label>IMAGE</label>
                                                    <IconUpload 
                                                    ref={this.imageRef}
                                                        defaultValue={imgUrl}
                                                        onSuccess={(image)=>{
                                                            this.setState({imgUrl:image.link});
                                                        }}
                                                    ></IconUpload>
                                                </FormGroup>
                                            </Col>
                                            <Col md={12}>
                                                <TextField
                                                    id="outlined-multiline-static"
                                                    label="Alt-Text Image"
                                                    style={{width:'100%',marginTop:20}}
                                                    multiline
                                                    rows={4}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }} 
                                                    value={remark} name="remark" onChange={this.handleChange}
                                                    variant="outlined"
                                                />
                                            </Col>
                                        </Row>
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={is_active}
                                                            onChange={this.handleChange}
                                                            name="is_active"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={is_comingsoon}
                                                            onChange={this.handleChange}
                                                            name="is_comingsoon"
                                                        />
                                                        }
                                                        label="IS COMINGSOON ?"
                                                    />
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={only_subscriber}
                                                            onChange={this.handleChange}
                                                            name="only_subscriber"
                                                        />
                                                        }
                                                        label="ONLY SUBSCRIBER"
                                                    />
                                                </FormGroup>
                                                
                                            </Col>
                                        </Row>
                                    </CardContent>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;