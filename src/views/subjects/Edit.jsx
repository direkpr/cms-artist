import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import {defaultService as service} from '../../services';
const _ = require('lodash');

const modelName ="subjects";
class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            is_active : true,
            icon1_url : undefined,
            title : undefined,
            icon2_url : undefined
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddTags = this.handleAddTags.bind(this);
        this.handleDeleteTags = this.handleDeleteTags.bind(this);
        this.handleReset = this.handleReset.bind(this);

        this.iconRef1 = React.createRef();
        this.iconRef2 = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "is_active"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleAddTags(tag){
        let {tags} = this.state;
        tags.push(tag);
        this.setState({tags});
    }
    handleDeleteTags(tag,index){
        let {tags} = this.state;
        tags = _.without(tags,tag);
        this.setState({tags});
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,title,submitted,is_active,icon1_url,icon2_url} = this.state;
        if(!title){
            confirmAlert({
                title: 'Error Message!',
                message: 'Level title  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let data = {
            id : id,
            title : title,
            icon1_url : icon1_url || "",
            icon2_url : icon2_url || "",
            is_active : is_active,
            createdtime : new Date().toISOString(),
            updatedtime : new Date().toISOString()
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Update data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            //this.componentDidMount();
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => this.setState({ error, submitted: false }))
        }
    }

    handleReset(e){
        e.preventDefault();
        this.componentDidMount();
        this.iconRef1.current.onReset();
        this.iconRef2.current.onReset();
    }
    componentDidMount(){
        service.getById(this.props.match.params.id,modelName).then(data=>{
            //console.log(data);
            this.setState(data);
        })
    }
    render(){
        const {id,submitted,error,is_active,icon1_url,icon2_url,title} = this.state;
        if(!id){
            return (<></>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                
                                    <CardHeader title="UPDATE SUBJECT">
                                    
                                    </CardHeader>
                                    <CardActions style={{display:"block",textAlign:"right"}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                    <CardContent>
                                        
                                        <Row>
                                            <Col md={12} lg={12}>
                                                <TextField required={true} style={{width:'100%',marginTop:10}} value={title} name="title" onChange={this.handleChange} label="TITLE" />
                                            </Col>
                                        </Row>
                                        <Row style={{paddingTop:15}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIcon">
                                                    <label>ICON NORMAL</label>
                                                    <IconUpload 
                                                    ref={this.iconRef1}
                                                        defaultValue={icon1_url}
                                                        onSuccess={(image)=>{
                                                            this.setState({icon1_url:image.link});
                                                        }}
                                                    ></IconUpload>
                                                </FormGroup>
                                            </Col>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIcon">
                                                    <label>ICON ACTIVE</label>
                                                    <IconUpload 
                                                    ref={this.iconRef2}
                                                        defaultValue={icon2_url}
                                                        onSuccess={(image)=>{
                                                            this.setState({icon2_url:image.link});
                                                        }}
                                                    ></IconUpload>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={is_active}
                                                            onChange={this.handleChange}
                                                            name="is_active"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                </FormGroup>
                                                
                                            </Col>
                                        </Row>
                                    </CardContent>
                                    
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;