import React from 'react';
import { Container as Grid, Row, Col } from "react-bootstrap";
import config from '../../config';
import MaterialTable from 'material-table';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import {authHeader,isAdmin} from '../../helpers'
import Icon from '@material-ui/core/Icon';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { FaEyeSlash,FaEye } from "react-icons/fa";
import * as moment from 'moment';
import {defaultService as service} from '../../services';
import { blue } from '@material-ui/core/colors';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import { ReactSortable } from "react-sortablejs";
import EditIcon from '@material-ui/icons/Edit';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';
const _ = require('lodash');
const modelName ="videos";
class VideoList extends React.Component {
    constructor(props){
        super(props);
        this.state = {}
        this.tableRef = React.createRef();
    }
    componentDidMount(){
        let json = {
            year :this.props.match.params.year
        }
        let url = config.apiUrl+"/api/videos?"
            url += '&limit=' + 100
            url += '&offset=' + 0
            url += '&search=' + '';
            url += '&where=' + JSON.stringify(json);
            url += '&orderby=a.order_item&orderdirection=asc';
            fetch(url,{
                method: 'GET',
                headers : authHeader()
            })
            .then(response => response.json())
            .then(result => {
                //console.log(result);
                this.setState({data:result.data});
            });
    }
    render(){
        let {data,album} = this.state;
        if(!data){
            return (<></>);
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>

                        <TableContainer component={Paper} >
                            <Toolbar>
                                <Typography variant="h6" component="h6">
                                    All music video in "{this.props.match.params.year}"
                                </Typography>
                            </Toolbar>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Price</TableCell>
                                    </TableRow>
                                </TableHead>
                                <ReactSortable tag="tbody" list={data} setList={(newslist)=>{
                                    this.setState({data:newslist});
                                    _.each(newslist,(row,key)=>{
                                        service.updateOrdeItem(row.id,key,modelName);
                                    })
                                }}>
                                {data.map((row,key) => (
                                    <TableRow style={{cursor:'move'}} key={key}>
                                        <TableCell component="th" scope="row">
                                            {
                                                row.name
                                            }
                                        </TableCell>
                                        <TableCell >
                                            {
                                                row.price
                                            }
                                        </TableCell>
                                    </TableRow>
                                ))}
                                </ReactSortable>
                            </Table>
                        </TableContainer>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
export default VideoList;