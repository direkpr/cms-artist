import React from 'react';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
  } from "react-bootstrap";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import * as moment from 'moment';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { CardContent } from '@material-ui/core';
import IconUpload from '../../components/IconUpload/IconUpload';
import Button from '@material-ui/core/Button';
import ChipInput from 'material-ui-chip-input'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormHelperText from '@material-ui/core/FormHelperText';
import {defaultService as service} from '../../services';
const _ = require('lodash');

const modelName ="videos";

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
}
function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box style={{paddingLeft:15,paddingRight:15}} p={3}>
            <Typography component="div">{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

class PageEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:undefined,
            submitted : false,
            isActive : true,
            coverUrl : undefined,
            thumUrl : undefined,
            link : undefined,
            stream_preview : undefined,
            stream_full : undefined,
            year : undefined,
            lg : 0,
            langs : null,
            texts : []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddTags = this.handleAddTags.bind(this);
        this.handleDeleteTags = this.handleDeleteTags.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.tabsHandleChange = this.tabsHandleChange.bind(this);

        this.iconRef1 = React.createRef();
        this.iconRef2 = React.createRef();
    }
    handleChange(e) {
        const { name, value,checked } = e.target;
        if(name === "isActive"){
            this.setState({ [name]: checked });
        }else{
            this.setState({ [name]: value });
        }
    }
    handleAddTags(tag){
        let {tags} = this.state;
        tags.push(tag);
        this.setState({tags});
    }
    handleDeleteTags(tag,index){
        let {tags} = this.state;
        tags = _.without(tags,tag);
        this.setState({tags});
    }
    handleSubmit(e) {
        e.preventDefault();
        const {id,year,link,stream_full,stream_preview,submitted,isActive,thumUrl,coverUrl,texts,langs} = this.state;
        if(!year){
            confirmAlert({
                title: 'Error Message!',
                message: 'Year  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!link){
            confirmAlert({
                title: 'Error Message!',
                message: 'Link purchase  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!stream_full){
            confirmAlert({
                title: 'Error Message!',
                message: 'URL Full  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!stream_preview){
            confirmAlert({
                title: 'Error Message!',
                message: 'URL Preview  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!coverUrl){
            confirmAlert({
                title: 'Error Message!',
                message: 'Cover iamge  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        if(!thumUrl){
            confirmAlert({
                title: 'Error Message!',
                message: 'Thumbnail image  has required.',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => {
                        return false;
                    }
                  }
                ]
            });
            return false;
        }
        let data = {
            id : id,
            link: link,
            year : year,
            stream_preview: stream_preview,
            stream_full : stream_full,
            coverUrl : coverUrl || "",
            thumUrl : thumUrl || "",
            isActive : isActive,
            texts : texts
        }
        if(!submitted){
            this.setState({ submitted: true });
            service.update(data,modelName).then(response=>{
                confirmAlert({
                    title: 'Message Box!',
                    message: 'Save data successful!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                            //window.location = this.props.path+"/edit/"+response.id;
                            window.location = this.props.path+"/list";
                        }
                    }
                    ]
                });
            },error => {
                this.setState({ error, submitted: false });
                confirmAlert({
                    title: 'Error Message!',
                    message: 'Save data fail!',
                    buttons: [
                    {
                        label: 'OK',
                        onClick: () => {
                            this.setState({ submitted: false });
                        }
                    }
                    ]
                });
            });
        }
        
    }

    handleReset(e){
        let {langs} = this.state;
        let texts = [];
        _.each(langs,(lang,key)=>{
            texts[key] = {lg:lang.code};
        })
        this.setState({isActive : true,
            coverUrl : undefined,
            thumUrl : undefined,
            year : undefined,
            link : undefined,
            stream_preview : undefined,
            stream_full : undefined,
            texts:texts
        });
        this.iconRef1.current.onReset();
        this.iconRef2.current.onReset();
        this.componentDidMount();
    }
    tabsHandleChange(event,value){
        this.setState({lg:value});
    }
    componentDidMount(){
        service.get_langs().then(langs=>{
            //console.log(langs);
            let texts = [];
            _.each(langs,(lang,key)=>{
                texts[key] = {lg:lang.code};
            })
            this.setState({langs,texts});
            service.getById(this.props.match.params.id,modelName).then((data)=>{
                _.each(langs,(lang,key)=>{
                    let idx = _.find(data.texts,["lg",lang.code]);
                    if(_.isUndefined(idx)){
                        data.texts.push({lg:lang.code})
                    }
                })
                data.isActive = (data.isActive == 1) ? true : false;
                this.setState(data);
            })
        })
    }
    render(){
        const {id,submitted,error,isActive,coverUrl,thumUrl,year,stream_preview,stream_full,link,lg,langs,texts} = this.state;
        if(!langs || !id){
            return (<></>);
        }
        //console.log(texts);
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Container>
                                <form onSubmit={this.handleSubmit} onReset={this.handleReset} autoComplete="off">
                                <Card style={{paddingLeft:20,paddingTop:30,paddingBottom:50}}>
                                
                                    <CardHeader title="UPDATE MUSIC VIDEO"></CardHeader>
                                    
                                    <CardContent>
                                        <Paper square>
                                        <Tabs
                                            value={lg}
                                            indicatorColor="primary"
                                            textColor="primary"
                                            variant="fullWidth"
                                            onChange={this.tabsHandleChange}
                                        >
                                            {
                                                (langs) && langs.map((lang,key)=>(
                                                    <Tab key={key} label={lang.title} {...a11yProps(key)} />
                                                ))
                                            }
                                            
                                        </Tabs>
                                        {
                                            (langs) && langs.map((lang,key)=>(
                                                <TabPanel key={key} value={lg} index={key}>
                                                    <Row>
                                                        <Col md={12} lg={12}>
                                                            <TextField required={(lang.isDefault == 1)} style={{width:'100%',marginTop:10}} defaultValue={texts[key].name} name="name" onChange={(e)=>{
                                                                texts[key].name = e.target.value;
                                                                this.setState({texts});
                                                            }} label="NAME" />
                                                        </Col>
                                                        <Col md={12} lg={12}>
                                                            <TextField required={(lang.isDefault == 1)} style={{width:'100%',marginTop:10}} defaultValue={texts[key].price} name="price" onChange={(e)=>{
                                                                texts[key].price = e.target.value;
                                                                this.setState({texts});
                                                            }} helperText="Price (include tax)" label="PRICE" />
                                                        </Col>
                                                    </Row>
                                                </TabPanel>
                                            ))
                                        }

                                            <Row style={{padding:15}}>
                                                <Col md={12} lg={12}>
                                                    <TextField required={true} style={{width:'100%',marginTop:10}} defaultValue={year} name="year" onChange={this.handleChange} label="YEAR" />
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField
                                                        helperText="URL for preivew demo video (30 sec.)"
                                                        required={true} style={{width:'100%',marginTop:10}} defaultValue={stream_preview} name="stream_preview" onChange={this.handleChange} label="PRVIVIEW URL" />
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField 
                                                        helperText="URL for preivew full video"
                                                        required={true} style={{width:'100%',marginTop:10}} defaultValue={stream_full} name="stream_full" onChange={this.handleChange} label="FULL URL" />
                                                </Col>
                                                <Col md={12} lg={12}>
                                                    <TextField 
                                                        helperText="Link to BASE app for purchase this video"
                                                        required={true} style={{width:'100%',marginTop:10}} defaultValue={link} name="link" onChange={this.handleChange} label="LINK" />
                                                </Col>
                                                
                                            </Row>
                                            <Row style={{padding:15}}>
                                                <Col md={12}>
                                                    <FormGroup controlId="formControlsIcon">
                                                        <label>COVER IMAGE *</label>
                                                        <IconUpload 
                                                            ref={this.iconRef1}
                                                            defaultValue={coverUrl}
                                                            onSuccess={(image)=>{
                                                                this.setState({coverUrl:image.link});
                                                            }}
                                                        ></IconUpload>
                                                        <FormHelperText>Image type: JPG file / Size: not over 1 mb / Dimension: width 1,925 px height 1,080 px</FormHelperText>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={12}>
                                                    <FormGroup controlId="formControlsIcon">
                                                        <label>THUMBNAIL IMAGE *</label>
                                                        <IconUpload 
                                                            ref={this.iconRef2}
                                                            defaultValue={thumUrl}
                                                            onSuccess={(image)=>{
                                                                this.setState({thumUrl:image.link});
                                                            }}
                                                        ></IconUpload>
                                                        <FormHelperText>Image type: JPG file / Size: not over 300 kb / Dimension: width 303 px height 170 px</FormHelperText>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                         </Paper>


                                        
                                        
                                        <Row style={{paddingTop:30}}>
                                            <Col md={12}>
                                                <FormGroup controlId="formControlsIsdefault">
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={isActive}
                                                            onChange={this.handleChange}
                                                            name="isActive"
                                                        />
                                                        }
                                                        label="Active"
                                                    />
                                                </FormGroup>
                                                
                                            </Col>
                                        </Row>
                                    </CardContent>
                                    <CardActions style={{padding:15}}>
                                        <Button disabled={submitted} size="small" type="reset" variant="contained" color="secondary">
                                        RESET
                                        </Button>
                                        <Button disabled={submitted} size="small" type="submit" variant="contained" color="primary">
                                        SUBMIT
                                        </Button>
                                    </CardActions>
                                </Card>
                                </form>
                            </Container>
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
export default PageEdit;