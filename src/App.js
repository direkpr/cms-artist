import React from 'react';
import {isLogin} from './helpers'
import PrivateLayout from './layouts/PrivateLayout'
import PublicLayout from './layouts/PublicLayout';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

function handleResponse(response) {
  return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
          if (response.status === 401) {
              // auto logout if 401 response returned from api
              
          }
          const error = (data.error && data.error.message) || response.statusText;
          return Promise.reject(error);
      }
      if(data.status === false){
          const error = data.message;
          return Promise.reject(error);
      }
      return data;
  });
}
function App(props) {
  return (
    <>
      {
        isLogin() && (
          <PrivateLayout {...props} />
        )
      }
      {
        !isLogin() && (
          <PublicLayout {...props} />
        )
      }
      
    </>
  );
}

export default App;
