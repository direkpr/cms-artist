import React, { Component,useState,useCallback } from "react";
import { authHeader } from "../../helpers";
import config from "../../config";
import styled from 'styled-components';
import {useDropzone} from 'react-dropzone';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

const _ = require('lodash');
const getColor = (props) => {
    if (props.isDragAccept) {
        return '#00e676';
    }
    if (props.isDragReject) {
        return '#ff1744';
    }
    if (props.isDragActive) {
        return '#2196f3';
    }
    return '#eeeeee';
}
  
const Container = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
    border-width: 2px;
    border-radius: 2px;
    border-color: ${props => getColor(props)};
    border-style: dashed;
    background-color: #fafafa;
    color: #bdbdbd;
    outline: none;
    transition: border .24s ease-in-out;
    margin-bottom: 20px;
    min-height : 78px;
`;

const ContainerPreview = styled.div`
    display : inline-block;
`;
const ImageBox = styled.div`
    display : block;
    float :left;
    padding : 5px;
    position : relative;
    width : 100%
`;

function StyledDropzone(props) {
    const {
      getRootProps,
      getInputProps,
      isDragActive,
      isDragAccept,
      isDragReject
    } = useDropzone({
        accept: 'image/*',
        onDropAccepted : files => {
            files.forEach(file=>{
                var formData = new FormData();
                formData.append('file', file);
                formData.append('librarySheetId',props.librarySheetId);
                fetch(props.urlupload, {
                    method: 'POST',
                    body: formData,
                    headers : authHeader()
                }).then((response) =>{
                    response.text().then(text =>{
                        let json = JSON.parse(text);
                        props.onUploadSucess(json);
                    });
                })
            })
        }
    });
    
    return (
        <Container {...getRootProps({isDragActive, isDragAccept, isDragReject})}>
            <input {...getInputProps()} />
            <p>Drag 'n' drop image files here, or click to select files.</p>
        </Container>
    );
}
export class ExerciseHintImage extends Component {
    constructor(props){
        super(props);
        this.state = {
            images : this.props.data || [],
            librarySheetId : this.props.librarySheetId,
            error : undefined
        }
        this.setData = this.setData.bind(this);
    }
    handleDelImages(image){
        let {images} = this.state;
        images = _.without(images,image)
        this.setState({images:images});
        if(this.props.onSuccess){
            this.props.onSuccess(images);
        }
    }
    setData(images){
        //console.log(images);
        if(!_.isEmpty(images)){
            images = _.sortBy(images,['name']);
            this.setState({images})
        }else{
            this.setState({images:[]})
        }
        
    }
    render() {
        let {images,error} = this.state
        return (
            <div>
                <StyledDropzone librarySheetId={this.props.librarySheetId}
                    urlupload={config.apiUrl+'/upload-hint-file'} 
                    onUploadSucess={(data)=>{
                        //console.log(data);
                        if(data.status){
                            images.push({
                                name : parseInt(data.data.name.split(".")[0]),
                                img_url : data.data.link
                            })
                            //console.log(images);
                            images = _.sortBy(images,['name']);
                            this.setState({images:images});
                            if(this.props.onSuccess){
                                this.props.onSuccess(images);
                            }
                        }else{
                            this.setState({error : data.message});
                        }
                    }}
                ></StyledDropzone>
                {
                    error &&
                    <div className={'alert alert-danger'}>{error}</div>
                }
                <ContainerPreview>
                {
                    (images) &&
                    images.map((image,key)=>
                        <ImageBox key={'img-key-'+key}>
                            <img alt="Image preview" src={image.img_url} style={{width:'100%'}} />
                            <p style={{position:"absolute",bottom: 10,left:10,marginBottom:0}}>#{image.name}</p>
                            <IconButton onClick={(e)=>{
                                this.handleDelImages(image);
                            }} style={{position:"absolute",bottom: 3,right:3}} aria-label="delete">
                                <DeleteIcon /> 
                            </IconButton>
                        </ImageBox>
                    )
                }
                </ContainerPreview>
            </div>
        )
    }
}

export default ExerciseHintImage;
