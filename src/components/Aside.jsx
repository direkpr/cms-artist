import React from 'react';
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from 'react-pro-sidebar';
import { Link } from 'react-router-dom';
import { FaTachometerAlt,FaMusic,FaBirthdayCake, FaImages,FaPhotoVideo, FaListAlt, FaNewspaper, FaShareAltSquare, FaHeart,FaSignOutAlt,FaTags,FaCode, } from 'react-icons/fa';

import sidebarBg from '../assets/img/bg1.jpg';
import {userService} from '../services'
import config from '../config';
import { useState } from 'react';
import { defaultUserRole } from '../helpers';

const RoleMenus = ((userRole)=>{
  if(userRole.role == "admin"){
    return (<>
    <Menu  iconShape="circle">
          <MenuItem 
            icon={<FaTachometerAlt />}
          >
            WELCOME
            <Link to="/cms/dashboard" />
          </MenuItem>
          <MenuItem icon={<FaImages />} >
            ARTIST PHOTO
            <Link to="/cms/photos" />
          </MenuItem>
          <MenuItem icon={<FaPhotoVideo />} >
            MUSIC VIDEO
            <Link to="/cms/videos" />
          </MenuItem>
          <SubMenu 
            title="MUSIC"
            icon={<FaMusic />}
          >
            <MenuItem>Albums
              <Link to="/cms/music/albums" />
            </MenuItem>
            <MenuItem>Songs
              <Link to="/cms/music/songs" />
            </MenuItem>
          </SubMenu>
          <MenuItem icon={<FaNewspaper />} >
            NEWS & EVENT
            <Link to="/cms/news" />
          </MenuItem>
          <MenuItem icon={<FaListAlt />} >
            GOODS
            <Link to="/cms/goods" />
          </MenuItem>
          <MenuItem icon={<FaListAlt />} >
            E-BIRTHDAY CARDS
            <Link to="/cms/ebirthday-cards" />
          </MenuItem>
          <MenuItem icon={<FaShareAltSquare />} >
            SOCIAL LINK
            <Link to="/cms/social-link" />
          </MenuItem>
          <MenuItem icon={<FaTags />} >
            META TAG
            <Link to="/cms/metetag" />
          </MenuItem>
          <MenuItem icon={<FaCode />} >
            MARKETING SCRIPTS
            <Link to="/cms/marketing-scripts" />
          </MenuItem>
          
          <SubMenu 
            title="REPORTS"
            icon={<FaListAlt />}
          >
            <MenuItem>Contacts
              <Link to="/cms/contacts" />
            </MenuItem>
            <MenuItem>Sign Up List
              <Link to="/cms/signup-list" />
            </MenuItem>
            <MenuItem>Monthly Subscription
              <Link to="/cms/recurring-list" />
            </MenuItem>
            <MenuItem>One time register
              <Link to="/cms/renew-list" />
            </MenuItem>
            <MenuItem>E-Birthday
              <Link to="/cms/e-birthday-report" />
            </MenuItem>
          </SubMenu>
          <MenuItem icon={<FaListAlt />} >
            MANAGE USER
            <Link to="/cms/user" />
          </MenuItem>
        </Menu>
    </>);
  }else if(userRole.role == "manager"){
    return (<>
      <Menu  iconShape="circle">
            <MenuItem 
              icon={<FaTachometerAlt />}
            >
              WELCOME
              <Link to="/cms/dashboard" />
            </MenuItem>
            <MenuItem icon={<FaImages />} >
              ARTIST PHOTO
              <Link to="/cms/photos" />
            </MenuItem>
            <MenuItem icon={<FaPhotoVideo />} >
              MUSIC VIDEO
              <Link to="/cms/videos" />
            </MenuItem>
            <SubMenu 
              title="MUSIC"
              icon={<FaMusic />}
            >
              <MenuItem>Albums
                <Link to="/cms/music/albums" />
              </MenuItem>
              <MenuItem>Songs
                <Link to="/cms/music/songs" />
              </MenuItem>
            </SubMenu>
            <MenuItem icon={<FaNewspaper />} >
              NEWS & EVENT
              <Link to="/cms/news" />
            </MenuItem>
            <MenuItem icon={<FaListAlt />} >
              GOODS
              <Link to="/cms/goods" />
            </MenuItem>
            <MenuItem icon={<FaListAlt />} >
              E-BIRTHDAY CARDS
              <Link to="/cms/ebirthday-cards" />
            </MenuItem>
            <MenuItem icon={<FaShareAltSquare />} >
              SOCIAL LINK
              <Link to="/cms/social-link" />
            </MenuItem>
            <MenuItem icon={<FaTags />} >
              META TAG
              <Link to="/cms/metetag" />
            </MenuItem>
            <MenuItem icon={<FaCode />} >
              MARKETING SCRIPTS
              <Link to="/cms/marketing-scripts" />
            </MenuItem>
            
          </Menu>
      </>);
  }else if(userRole.role == "report"){
    return (<>
      <Menu  iconShape="circle">
            <MenuItem 
              icon={<FaTachometerAlt />}
            >
              WELCOME
              <Link to="/cms/dashboard" />
            </MenuItem>
            
            <SubMenu 
              title="REPORTS"
              icon={<FaListAlt />}
            >
              <MenuItem>Contacts
                <Link to="/cms/contacts" />
              </MenuItem>
              <MenuItem>Sign Up List
                <Link to="/cms/signup-list" />
              </MenuItem>
              <MenuItem>Monthly Subscription
                <Link to="/cms/recurring-list" />
              </MenuItem>
              <MenuItem>One time register
                <Link to="/cms/renew-list" />
              </MenuItem>
              <MenuItem>E-Birthday
                <Link to="/cms/e-birthday-report" />
              </MenuItem>
            </SubMenu>
          </Menu>
      </>);
  }else{
    return (<></>)
  }
})


const Aside = ({ image, collapsed, rtl, toggled, handleToggleSidebar }) => {
  const [userRole,setUserRole] = useState(defaultUserRole())
  //console.log(userRole);
  return (
    <ProSidebar
      image={sidebarBg}
      rtl={rtl}
      collapsed={collapsed}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}
    >
      <SidebarHeader>
        <div
          style={{
            padding: '24px',
            textTransform: 'uppercase',
            fontWeight: 'bold',
            fontSize: 14,
            letterSpacing: '1px',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
          }}
        >
          REALME - AYANA MIYAKE
        </div>
      </SidebarHeader>

      <SidebarContent>
        <RoleMenus role={userRole} />
      </SidebarContent>

      <SidebarFooter style={{ textAlign: 'center' }}>
        <div
          className="sidebar-btn-wrapper"
          style={{
            padding: '20px 24px',
          }}
        >
          <a
            onClick={(e)=>{
              userService.logout();
              window.location = config.baseUrl+"/cms/logout";
            }}
            className="sidebar-btn"
            rel="noopener noreferrer" style={{cursor:'pointer'}}
          >
            <FaSignOutAlt />
            <span> LOGOUT</span>
          </a>
        </div>
      </SidebarFooter>
    </ProSidebar>
  );
};

export default Aside;