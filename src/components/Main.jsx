import React from 'react';
import { Route, Switch,BrowserRouter as Router, Redirect } from "react-router-dom";
import { FaHeart, FaBars } from 'react-icons/fa';
import reactLogo from '../assets/img/logo.png';
import {allRoutes,adminRoutes,managerRoutes,reportRoutes} from '../routes'
import { isAdmin, isManager, isReport } from '../helpers';


const getRoutes = routes => {
  return routes.map((prop, key) => {
    return (
      <Route
        path={prop.path}
        render={props => (
          <prop.component path={prop.path}
            {...props}
          />
        )}
        key={key}
      />
    );
  });
};
const Main = (props) => {
  let routes = [];
  if(isAdmin()){
    routes = adminRoutes;
  }
  if(isManager()){
    routes = managerRoutes;
  }
  if(isReport()){
    routes = reportRoutes;
  }
  

  let getBrandText = function (pathname){
    for (let i = 0; i < routes.length; i++) {
      if (
        props.location.pathname.indexOf(
          routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Dashboard";
  }

  
  return (
    <main>
      <div className="btn-toggle" onClick={() =>props.handleToggleSidebar(true)}>
        <FaBars />
      </div>
      <header>
        <h1 style={{paddingLeft:20}}>
        {getBrandText(props.location.pathname)}
        </h1>
      </header>
      <Switch>
        {getRoutes(routes)}
        <Redirect path="/cms/" to="/cms/dashboard" />
      </Switch>

      <footer>
        <small>
          © 2020 REALME - AYANA MIYAKE. All rights reserved.
        </small>
      </footer>
    </main>
  );
};
export default Main;