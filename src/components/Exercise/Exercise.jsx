import React, { Component } from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import IconButton from '@material-ui/core/IconButton';
import {FaMinusCircle} from "react-icons/fa";
import Checkbox from '@material-ui/core/Checkbox';

import {
    Container as Grid,
    Row,
    Col,
  } from "react-bootstrap";


  export class Exercises extends Component {
    constructor(props){
        super(props);
        this.state = {
            exercise : props.exercise || null,
            index : props.index || -1
        }
    }
    render(){
        let {exercise} = this.state;
        let answerlist_el = "";
        var tmp = [];
        for(let i = 0; i < exercise.num_of_answer; i ++){
            tmp.push(i);
        }
        var indents = tmp.map(function (i,k) {
            return (
                <FormControlLabel key={`key-option-${k}`}
                    value={i}
                    control={<Radio color="primary" />}
                    label={i}
                    labelPlacement="end"
                    />
            );
        });
        //console.log(exercise);
        return (
            <div>
                <Row>
                    <Col md={12}>
                        <FormControl component="fieldset">
                            <FormGroup aria-label="position" row>
                                <FormLabel component="div" style={{marginTop:'17px',marginRight:'15px'}}><strong>#{exercise.ex_no}</strong></FormLabel>
                                <RadioGroup style={{paddingTop:5}} onChange={(e,v)=>{
                                    exercise.answer = parseInt(v);
                                    this.setState({exercise});
                                    if(this.props.onUpdateAnswer){
                                        this.props.onUpdateAnswer(exercise);
                                    }
                                }} row aria-label="position" name="position" value={exercise.answer} >
                                    {indents}
                                </RadioGroup>
                                <FormControlLabel style={{paddingTop:5}}
                                    value={exercise.have_hint}
                                    control={<Checkbox checked={exercise.have_hint} color="primary" />}
                                    label="Hint?"
                                    labelPlacement="end"
                                     onChange={(e,v)=>{
                                        exercise.have_hint = v;
                                        this.setState({exercise});
                                        if(this.props.onUpdateAnswer){
                                            this.props.onUpdateAnswer(exercise);
                                        }
                                     }}
                                    />
                                <IconButton  onClick={(e)=>{
                                        if(this.props.onDelete){
                                            this.props.onDelete(exercise);
                                        }
                                    }} color="secondary" aria-label="upload picture"  variant="contained" component="span">
                                    <FaMinusCircle />
                                </IconButton>

                            </FormGroup>
                        </FormControl>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Exercises;