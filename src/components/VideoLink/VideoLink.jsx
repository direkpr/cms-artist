import React,{Component} from 'react';
import {userService} from '../../services'
import config from '../../config';
import { TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import {FaMinusCircle,FaPlusCircle} from "react-icons/fa";
import Card from '@material-ui/core/Card';
import IconUpload from '../IconUpload/IconUpload';
import ChipInput from 'material-ui-chip-input'
import CardHeader from '@material-ui/core/CardHeader';
import {
    Container as Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel
} from "react-bootstrap";
import { ReactSortable } from "react-sortablejs";
const _ = require('lodash');

export class VideoLink extends Component {
    constructor(props){
        super(props);
        this.state = {
            videos : [{link:"",title:"",tags:[]}]
        }
        this.onReset = this.onReset.bind(this);
        this.setFiles = this.setFiles.bind(this);
    }
    componentDidMount(){
        //console.log(this.props.defaultValue);
        if(this.props.defaultValue){
            this.setState({videos:this.props.defaultValue});
        }
    }
    setFiles(videos){
        _.each(videos,(video,vk)=>{
            if(video.tag.length > 0){
                video.tags = video.tag.split(",") || [];
            }
        });
        if(_.isUndefined(videos) || _.isNull(videos)){
            videos = [{link:"",title:"",tags:[]}];
        }
        this.setState({videos : videos});
    }
    onReset(){
        this.setState({videos : [{link:"",title:"",tags:[]}]});   
    }
    render() {
        const {videos} = this.state
        return (
            <FormGroup>
                <label>ALL VIDEO</label>
                <ReactSortable list={videos} setList={(newVals)=>{
                    //console.log(newVals);
                    this.setState({videos:newVals});
                    if(this.props.onUpdate){
                        this.props.onUpdate(newVals);
                    }
                }}>
                {
                    (videos) && videos.map((video,k)=>(
                        <Card key={`video-link-${k}`} style={{paddingLeft:15,paddingBottom:10,marginTop:10,marginBottom:10,cursor:"move"}}>
                            <CardHeader title={`VIDOE #${(k+1)}`}></CardHeader>
                            <Row>
                                <Col md={10}>
                                    <Row>
                                        <Col md={12}>
                                            <TextField style={{width:'100%',marginTop:10}} value={video.title} onChange={(e)=>{
                                                    videos[k].title = e.target.value;
                                                    this.setState({videos});
                                                    if(this.props.onUpdate){
                                                        this.props.onUpdate(videos);
                                                    }
                                                }} label="Video Title" />
                                        </Col>
                                        <Col md={12}>
                                            <ChipInput
                                                label="Video Tags" 
                                                style={{width:'100%',marginTop:10}}
                                                value={video.tags}
                                                onAdd={(chip) => {
                                                    videos[k].tags.push(chip);
                                                    this.setState({videos});
                                                    if(this.props.onUpdate){
                                                        this.props.onUpdate(videos);
                                                    }
                                                }}
                                                onDelete={(chip, index) => {
                                                    videos[k].tags = _.without(videos[k].tags,chip);
                                                    this.setState({videos});
                                                    if(this.props.onUpdate){
                                                        this.props.onUpdate(videos);
                                                    }
                                                }}
                                                />
                                        </Col>
                                        <Col md={12}>
                                            <TextField style={{width:'100%',marginTop:10}} value={video.link} onChange={(e)=>{
                                                videos[k].link = e.target.value;
                                                this.setState({videos});
                                                if(this.props.onUpdate){
                                                    this.props.onUpdate(videos);
                                                }
                                                }} label="Video Link" />
                                        </Col>
                                        <Col style={{paddingTop:20}} md={12}>
                                            <FormGroup controlId="formControlsIcon">
                                                <label>Cover Image</label>
                                                <IconUpload 
                                                    defaultValue={video.imgUrl}
                                                    onSuccess={(image)=>{
                                                        videos[k].imgUrl = image.link
                                                        this.setState({videos});
                                                        if(this.props.onUpdate){
                                                            this.props.onUpdate(videos);
                                                        }
                                                    }}
                                                ></IconUpload>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={2}>
                                    <IconButton style={{top:'50%',transform: 'translateY(-30%)'}} onClick={(e)=>{
                                        //delete videos[k];
                                        let tmp = _.without(videos,video);
                                        //console.log(tmp);
                                        this.setState({videos:tmp});
                                        if(this.props.onUpdate){
                                            this.props.onUpdate(tmp);
                                        }
                                    }} color="secondary" aria-label="upload picture"  variant="contained" component="span">
                                        <FaMinusCircle />
                                    </IconButton>
                                </Col>
                            </Row>
                        </Card>
                    ))
                }
                </ReactSortable>
                <Row style={{marginTop:30}}>
                    <Col>
                        <Button
                            onClick={(e)=>{
                                videos.push({link:"",title:"",tags:[]});
                                this.setState({videos});
                                if(this.props.onUpdate){
                                    this.props.onUpdate(videos);
                                }
                            }}
                            variant="contained"
                            color="primary"
                            size="small"
                            startIcon={<FaPlusCircle />}
                        >
                            Add Video
                        </Button>
                    </Col>
                </Row>
                
            </FormGroup>
          );
    }
}

export default VideoLink;