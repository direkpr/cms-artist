import React, { Component } from "react";
import { authHeader } from "../../helpers";
import config from "../../config";
import styled from 'styled-components';
import {useDropzone} from 'react-dropzone';

const getColor = (props) => {
    if (props.isDragAccept) {
        return '#00e676';
    }
    if (props.isDragReject) {
        return '#ff1744';
    }
    if (props.isDragActive) {
        return '#2196f3';
    }
    return '#eeeeee';
}
  
const Container = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
    border-width: 2px;
    border-radius: 2px;
    border-color: ${props => getColor(props)};
    border-style: dashed;
    background-color: #fafafa;
    color: #bdbdbd;
    outline: none;
    transition: border .24s ease-in-out;
    margin-bottom: 20px;
    min-height : 78px;
`;
function StyledDropzone(props) {
    const {
      getRootProps,
      getInputProps,
      isDragActive,
      isDragAccept,
      isDragReject
    } = useDropzone({
        accept: 'image/*',
        onDropAccepted : files => {
            files.forEach(file=>{
                var formData = new FormData();
                formData.append('file', file);
                fetch(props.urlupload, {
                    method: 'POST',
                    body: formData,
                    headers : authHeader()
                }).then((response) =>{
                    response.text().then(text =>{
                        let json = JSON.parse(text);
                        props.onUploadSucess(json);
                    });
                })
            })
        }
    });
    
    return (
        <Container {...getRootProps({isDragActive, isDragAccept, isDragReject})}>
            <input {...getInputProps()} />
            <p>Drag 'n' drop image files here, or click to select files.</p>
        </Container>
    );
}
export class IconUpload extends Component {
    constructor(props){
        super(props);
        this.state = {
            previewUrl : undefined,
            error : undefined
        }
        this.onReset = this.onReset.bind(this);
    }
    componentDidMount(){
        //console.log(this.props);
    }
    onReset(){
        this.setState({
            previewUrl : undefined,
            error : undefined
        });
    }
    render() {
        const {previewUrl,error} = this.state
        //console.log(previewUrl);
        return (
            <div>
                <StyledDropzone 
                    urlupload={config.apiUrl+'/api/upload-file'} 
                    onUploadSucess={(data)=>{
                        if(data.link){
                            this.setState({previewUrl:data.link});
                            if(this.props.onSuccess)
                                this.props.onSuccess(data);
                        }else{
                            this.setState({error :"Upload fail"});
                        }
                    }}
                ></StyledDropzone>
                {
                    error &&
                    <div className={'alert alert-danger'}>{error}</div>
                }
                {
                    (this.props.defaultValue || previewUrl) &&
                    <img alt="Icon preview" src={config.imgUrl+(this.props.defaultValue || previewUrl)} style={{height:100}} />
                }
            </div>
        )
    }
}

export default IconUpload;
