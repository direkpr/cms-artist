import React, { Component } from "react";
import { authHeader } from "../../helpers";
import config from "../../config";
import styled from 'styled-components';
import {useDropzone} from 'react-dropzone';
import {FaFilePdf} from "react-icons/fa";

const getColor = (props) => {
    if (props.isDragAccept) {
        return '#00e676';
    }
    if (props.isDragReject) {
        return '#ff1744';
    }
    if (props.isDragActive) {
        return '#2196f3';
    }
    return '#eeeeee';
}
  
const Container = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
    border-width: 2px;
    border-radius: 2px;
    border-color: ${props => getColor(props)};
    border-style: dashed;
    background-color: #fafafa;
    color: #bdbdbd;
    outline: none;
    transition: border .24s ease-in-out;
    margin-bottom: 20px;
    min-height : 78px;
`;
function StyledDropzone(props) {
    const {
      getRootProps,
      getInputProps,
      isDragActive,
      isDragAccept,
      isDragReject
    } = useDropzone({
        accept: 'application/pdf',
        onDropAccepted : files => {
            files.forEach(file=>{
                var formData = new FormData();
                formData.append('file', file);
                fetch(props.urlupload, {
                    method: 'POST',
                    body: formData,
                    headers : authHeader()
                }).then((response) =>{
                    response.text().then(text =>{
                        let json = JSON.parse(text);
                        props.onUploadSucess(json);
                    });
                })
            })
        }
    });
    
    return (
        <Container {...getRootProps({isDragActive, isDragAccept, isDragReject})}>
            <input {...getInputProps()} />
            <p>Drag 'n' drop library sheet files here, or click to select files.</p>
        </Container>
    );
}
export class SheetFileUpload extends Component {
    constructor(props){
        super(props);
        this.state = {
            file : undefined,
            error : undefined
        }
        this.onReset = this.onReset.bind(this);
        this.setFile = this.setFile.bind(this);
    }
    componentDidMount(){
        //console.log(this.props.defaultValue);
        this.setState({file:this.props.defaultValue});
    }
    onReset(){
        this.setState({file:undefined,error:undefined});
        if(this.props.onSuccess)
            this.props.onSuccess(null);
    }
    setFile(file){
        this.setState({file});
    }
    render() {
        const {file,error} = this.state
        //console.log(file);
        return (
            <div>
                <StyledDropzone 
                    urlupload={config.apiUrl+'/upload-sheet-file'} 
                    onUploadSucess={(data)=>{
                        if(data.status){
                            this.setState({file:data.data});
                            if(this.props.onSuccess)
                                this.props.onSuccess(data.data);
                        }else{
                            this.setState({error : data.message});
                        }
                    }}
                ></StyledDropzone>
                {
                    error &&
                    <div className={'alert alert-danger'}>{error}</div>
                }
                {
                    (file) &&
                    (
                        <a href={file.link || file.path} target="_blank">
                            <FaFilePdf size={38} />{file.filename || file.name}
                        </a>
                    )
                }
            </div>
        )
    }
}

export default SheetFileUpload;
