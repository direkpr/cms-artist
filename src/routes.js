import {userService} from './helpers'
import Dashboard from './views/dashboard/Page';
import { FaTachometerAlt, FaGem, FaList, FaGithub, FaRegLaughWink, FaHeart,FaSignOutAlt,FaBuffer } from 'react-icons/fa';
import Goods from './views/goods/Page';
import SocialLink from './views/social-link/SocialLink';
import Photo from './views/photo/Page';
import Album from './views/album/Page';
import Song from './views/song/Page';
import Video from './views/video/Page';
import News from './views/news/Page';
import Metetag from './views/metetag/Metetag';
import MarketingScript from './views/marketing-script/MarketingScript';
import Contact from './views/contact/Page';
import SignupList from './views/signup-list/Page';
import RecurringList from './views/recurring-list/Page';
import Renew from './views/renew-list/Page';
import EBirthdayCard from './views/ebirthday-cards/Page';
import EBirthdayList from './views/ebirthday-list/Page';
import UserList from './views/user/Page';


export const allRoutes = [
    {
        path : "/cms/dashboard",
        name:"Dashboard",
        icon: FaTachometerAlt,
        component : Dashboard
    },
];

export const adminRoutes = [

    {
        path : "/cms/dashboard",
        name:"Welcome",
        icon: FaTachometerAlt,
        component : Dashboard
    },
    {
        path : "/cms/goods",
        name : "Goods",
        icon : FaBuffer,
        component : Goods
    },
    {
        path : "/cms/photos",
        name : "Artist Photo",
        icon : FaBuffer,
        component : Photo
    },
    {
        path : "/cms/news",
        name : "News & Event",
        icon : FaBuffer,
        component : News
    },
    {
        path : "/cms/videos",
        name : "Music Video",
        icon : FaBuffer,
        component : Video
    },
    {
        path : "/cms/social-link",
        name : 'Social Link',
        icon : FaBuffer,
        component : SocialLink
    },
    {
        path : "/cms/marketing-scripts",
        name : 'Marketing Scripts',
        icon : FaBuffer,
        component : MarketingScript
    },
    {
        path : "/cms/metetag",
        name : 'Meta Tag',
        icon : FaBuffer,
        component : Metetag
    },
    {
        path : "/cms/music/albums",
        name : 'Albums',
        icon : FaBuffer,
        component : Album
    },
    {
        path : "/cms/music/songs",
        name : 'Songs',
        icon : FaBuffer,
        component : Song
    },
    {
        path : "/cms/contacts",
        name : 'Contacts',
        icon : FaBuffer,
        component : Contact
    },
    {
        path : "/cms/signup-list",
        name : 'Sign Up List',
        icon : FaBuffer,
        component : SignupList
    },
    {
        path : "/cms/recurring-list",
        name : 'Monthly Subscription',
        icon : FaBuffer,
        component : RecurringList
    },
    {
        path : "/cms/renew-list",
        name : 'One time register',
        icon : FaBuffer,
        component : Renew
    },
    {
        path : "/cms/ebirthday-cards",
        name : 'eBirthday Cards',
        icon : FaBuffer,
        component : EBirthdayCard
    },
    {
        path : "/cms/e-birthday-report",
        name : 'eBirthday',
        icon : FaBuffer,
        component : EBirthdayList
    },
    {
        path : "/cms/user",
        name : 'Manage User',
        icon : FaBuffer,
        component : UserList
    },
];

export const managerRoutes = [

    {
        path : "/cms/dashboard",
        name:"Welcome",
        icon: FaTachometerAlt,
        component : Dashboard
    },
    {
        path : "/cms/goods",
        name : "Goods",
        icon : FaBuffer,
        component : Goods
    },
    {
        path : "/cms/photos",
        name : "Artist Photo",
        icon : FaBuffer,
        component : Photo
    },
    {
        path : "/cms/news",
        name : "News & Event",
        icon : FaBuffer,
        component : News
    },
    {
        path : "/cms/videos",
        name : "Music Video",
        icon : FaBuffer,
        component : Video
    },
    {
        path : "/cms/social-link",
        name : 'Social Link',
        icon : FaBuffer,
        component : SocialLink
    },
    {
        path : "/cms/marketing-scripts",
        name : 'Marketing Scripts',
        icon : FaBuffer,
        component : MarketingScript
    },
    {
        path : "/cms/metetag",
        name : 'Meta Tag',
        icon : FaBuffer,
        component : Metetag
    },
    {
        path : "/cms/music/albums",
        name : 'Albums',
        icon : FaBuffer,
        component : Album
    },
    {
        path : "/cms/music/songs",
        name : 'Songs',
        icon : FaBuffer,
        component : Song
    }
];

export const reportRoutes = [

    {
        path : "/cms/dashboard",
        name:"Welcome",
        icon: FaTachometerAlt,
        component : Dashboard
    },
    {
        path : "/cms/contacts",
        name : 'Contacts',
        icon : FaBuffer,
        component : Contact
    },
    {
        path : "/cms/signup-list",
        name : 'Sign Up List',
        icon : FaBuffer,
        component : SignupList
    },
    {
        path : "/cms/recurring-list",
        name : 'Monthly Subscription',
        icon : FaBuffer,
        component : RecurringList
    },
    {
        path : "/cms/renew-list",
        name : 'One time register',
        icon : FaBuffer,
        component : Renew
    },
    {
        path : "/cms/ebirthday-cards",
        name : 'eBirthday Cards',
        icon : FaBuffer,
        component : EBirthdayCard
    },
    {
        path : "/cms/e-birthday-report",
        name : 'eBirthday',
        icon : FaBuffer,
        component : EBirthdayList
    }
];
export default allRoutes;